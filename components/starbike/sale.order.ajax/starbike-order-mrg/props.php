<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?
/*********** start props format ***********/ 
if (!function_exists("showFilePropertyField"))
{
	function showFilePropertyField($name, $property_fields, $values, $max_file_size_show=50000)
	{
		$res = "";

		if (!is_array($values) || empty($values))
			$values = array(
				"n0" => 0,
			);

		if ($property_fields["MULTIPLE"] == "N")
		{
			$res = "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
		}
		else
		{
			$res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
			$res .= "<br/><br/>";
			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[1]\" id=\"".$name."[1]\" onChange=\"javascript:addControl(this);\"></label>";
		}

		return $res;
	}
}

if (!function_exists("PrintPropsForm"))
{
	function PrintPropsForm($arSource = array(), $locationTemplate = ".default", $arResultOrderProps = array(), $arResultOrderPropsPrint = array())
	{
		if (!empty($arSource))
		{
			?>
				<div>
					<?
					foreach($arSource as $arProperties)
					{
						if ($arProperties["TYPE"] == "CHECKBOX")
						{
							?>
							<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value="">

							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r1x3 pt8"><input type="checkbox" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="Y"<?if ($arProperties["CHECKED"]=="Y") echo " checked";?>></div>

							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXT")
						{
							?>
							<?if($arProperties["FIELD_NAME"] == "ORDER_PROP_20" && $arResultOrderProps["23"] == "RUSSIA"):?>
                                <input type="text" maxlength="250" size="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="<?=GetMessage("RUSSIA")?>" style="display:none;">
							<?elseif($arProperties["FIELD_NAME"] == "ORDER_PROP_6" && $arResultOrderPropsPrint["23"]["VALUE"] == GetMessage("MOSCOW")):?>
                                <input type="text" maxlength="250" size="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="<?=GetMessage("MOSCOW")?>" style="display:none;">
							<?else:?>
                                <div class="bx_block r1x3 pt8">
                                    <?=$arProperties["NAME"]?>
                                    <?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
                                        <span class="bx_sof_req">*</span>
                                    <?endif;?>
                                </div>

                                <div class="bx_block r3x1">
                                    <input type="text" maxlength="250" size="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="<?=$arProperties["VALUE"]?>">
                                </div>
                                <div style="clear: both;"></div><br>
                            <?endif;?>
							<?
						}
						elseif ($arProperties["TYPE"] == "SELECT")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<select name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
									<?
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
									<?
									endforeach;
									?>
								</select>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "MULTISELECT")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<select multiple name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
									<?
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
									<?
									endforeach;
									?>
								</select>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXTAREA")
						{
							$rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<textarea rows="<?=$rows?>" cols="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"></textarea>
							</div>
							<div style="clear: both;"></div>
							<br>
							<?
						}
						elseif ($arProperties["TYPE"] == "LOCATION")
						{
							?>
							<div class="bx_block r1x3 pt8">
							</div>

							<div class="bx_block r3x1" style="display:none;">
								<?
                                $arProperties["VARIANTS"]["1"]["SELECTED"] = "Y";
								$GLOBALS["APPLICATION"]->IncludeComponent(
									"bitrix:sale.ajax.locations",
									$locationTemplate,
									array(
										"AJAX_CALL" => "N",
										"COUNTRY_INPUT_NAME" => "COUNTRY",
										"REGION_INPUT_NAME" => "REGION",
										"CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
										"CITY_OUT_LOCATION" => "Y",
										"LOCATION_VALUE" => 548,
										"ORDER_PROPS_ID" => $arProperties["ID"],
                                        "COUNTRY" => 5,
										"ONCITYCHANGE" => "",
										"SIZE1" => $arProperties["SIZE1"],
									),
									null,
									array('HIDE_ICONS' => 'Y')
								);
								?>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "RADIO")
						{
							?>
							 <?if (strpos($arProperties["CODE"], "DELIVERY_TYPE") === 0) /* for DELIVERY_TYPE property */:?>
                               
                                <div class="bx_block r1x3 pt8">
                                    <?=$arProperties["NAME"]?>
                                    <?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
                                        <span class="bx_sof_req">*</span>
                                    <?endif;?>
                                </div>

                                <div class="bx_block r3x1">
                                    <?
                                    if (is_array($arProperties["VARIANTS"]))
                                    {
                                        
                                        foreach($arProperties["VARIANTS"] as $arVariants):
                                        ?>
                                            <input
                                                type="radio"
                                                name="<?=$arProperties["FIELD_NAME"]?>"
                                                id="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"
                                                onchange="submitForm()"
                                                value="<?=$arVariants["VALUE"]?>" <?if($arVariants["CHECKED"] == "Y") echo " checked";?> />
                                            <label for="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"><?=$arVariants["NAME"]?></label><br>
                                        <?
                                        endforeach;
                                    }
                                    ?>
                                </div>
                                <div style="clear: both;"></div>
							 <?else:?>
                                <div class="bx_block r1x3 pt8">
                                    <?=$arProperties["NAME"]?>
                                    <?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
                                        <span class="bx_sof_req">*</span>
                                    <?endif;?>
                                </div>

                                <div class="bx_block r3x1">
                                    <?
                                    if (is_array($arProperties["VARIANTS"]))
                                    {
                                        foreach($arProperties["VARIANTS"] as $arVariants):
                                        ?>
                                            <input
                                                type="radio"
                                                name="<?=$arProperties["FIELD_NAME"]?>"
                                                id="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"
                                                value="<?=$arVariants["VALUE"]?>" <?if($arVariants["CHECKED"] == "Y") echo " checked";?> />

                                            <label for="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"><?=$arVariants["NAME"]?></label></br>
                                        <?
                                        endforeach;
                                    }
                                    ?>
                                </div>
                                <div style="clear: both;"></div>
							<?endif;?>
							<?
						}
						elseif ($arProperties["TYPE"] == "FILE")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<?=showFilePropertyField("ORDER_PROP_".$arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"])?>
							</div>

							<div style="clear: both;"></div><br/>
							<?
						}
					}
					?>
				</div>
			<?
		}
	}
}
/*********** end props format ***********/ ?>

<h1><?=GetMessage("SOA")?></h1>

<div class="bx_section">
    <h4><?=GetMessage("SOA_TEMPL_DELIVERY_CHOOSE")?></h4>
        <?PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_N"], $arParams["TEMPLATE_LOCATION"], $arResult["USER_VALS"]["ORDER_PROP"], $arResult["ORDER_PROP"]["PRINT"]);?>
<br>
<? /*********** start delivery ***********/ ?>
<script type="text/javascript">
    
	function fShowStore(id, showImages, formWidth, siteId)
	{
		var strUrl = '<?=$templateFolder?>' + '/map.php';
		var strUrlPost = 'delivery=' + id + '&showImages=' + showImages + '&siteId=' + siteId;

		var storeForm = new BX.CDialog({
					'title': '<?=GetMessage('SOA_ORDER_GIVE')?>',
					head: '',
					'content_url': strUrl,
					'content_post': strUrlPost,
					'width': formWidth,
					'height':450,
					'resizable':false,
					'draggable':false
				});

		var button = [
				{
					title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
					id: 'crmOk',
					'action': function ()
					{
						GetBuyerStore();
						BX.WindowManager.Get().Close();
					}
				},
				BX.CDialog.btnCancel
			];
		storeForm.ClearButtons();
		storeForm.SetButtons(button);
		storeForm.Show();
	}

	function GetBuyerStore()
	{
		BX('BUYER_STORE').value = BX('POPUP_STORE_ID').value;
		BX('store_desc').innerHTML = BX('POPUP_STORE_NAME').value;
		BX.show(BX('select_store'));
	}
</script>

<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?=$arResult["BUYER_STORE"]?>" />
<div class="bx_section delivery-options">
	<?
	if(!empty($arResult["DELIVERY"]))
	{
		$width = ($arParams["SHOW_STORES_IMAGES"] == "Y") ? 850 : 700;

        foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
		{
            foreach ($arResult["ORDER_PROP"]["USER_PROPS_N"]["23"]["VARIANTS"] as $delivery_type_id => $arDeliveryType)
            {
                if (($arDeliveryType["NAME"] == $arResult["ORDER_PROP"]["USER_PROPS_N"]["23"]["VALUE_FORMATED"]) && ($arDelivery["NAME"] == $arDeliveryType["DESCRIPTION"]))
                {
                    $delivery_unique_coincidence = true; 
                    $delivery_unique_coincidence_id = $arDelivery["ID"];
                } else if (($arDeliveryType["NAME"] == $arResult["ORDER_PROP"]["USER_PROPS_N"]["23"]["VALUE_FORMATED"]) && ((strpos($arDeliveryType["DESCRIPTION"], $arDelivery["NAME"]) !== false))) {
                    $delivery_coincidence = true; 
                    $delivery_coincidence_array[$arDelivery["ID"]] = $arDelivery;
                }
            }
        }

        if ($delivery_unique_coincidence){
            foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
            {
                if ($delivery_unique_coincidence_id == $arDelivery["ID"]) {
                    if (count($arDelivery["STORE"]) > 0)
                        $clickHandler = "onClick = \"fShowStore('".$arDelivery["ID"]."','".$arParams["SHOW_STORES_IMAGES"]."','".$width."','".SITE_ID."')\";";
                    else
                        $clickHandler = "onClick = \"BX('ID_DELIVERY_ID_".$arDelivery["ID"]."').checked=true;submitForm();\"";
                    ?>
                        <div class="row <?if ($arDelivery['CHECKED']=='Y') echo ' active';?>" <?=$clickHandler?>>
                                <input type="radio"
                                    id="ID_DELIVERY_ID_<?=$arDelivery["ID"]?>"
                                    name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
                                    value="<?=$arDelivery["ID"]?>" checked                                     
                                    />
                                    <?
                                    if (count($arDelivery["LOGOTIP"]) > 0):

                                        $arFileTmp = CFile::ResizeImageGet(
                                            $arDelivery["LOGOTIP"]["ID"],
                                            array("width" => "95", "height" =>"55"),
                                            BX_RESIZE_IMAGE_PROPORTIONAL,
                                            true
                                        );

                                        $deliveryImgURL = $arFileTmp["src"];
                                    else:
                                        $deliveryImgURL = $templateFolder."/images/logo-default-d.gif";
                                    endif;
                                    ?>

								<div class="columns large-2 delivery-img">
									<img src="<?=$deliveryImgURL?>"/>
								</div>

								<div class="columns large-10">
                                        <strong>
									    <span class="bx_result_price">
											<?
											if (strlen($arDelivery["PERIOD_TEXT"])>0)
											{
												echo $arDelivery["PERIOD_TEXT"];
												?><br /><?
											}
											?>
                                                <?if($arDelivery["PRICE"] == 0):?>
                                                    <?=GetMessage("SALE_DELIV_FREE");?>
                                                <?else:?>
                                                    <?=GetMessage("SALE_DELIV_PRICE");?> <?=$arDelivery["PRICE_FORMATED"]?><br />
                                                <?endif;?>
										</span>
                                            <div class="name">
                                            <?= htmlspecialcharsbx($arDelivery["NAME"]) ?></div>
                                        </strong>
                                        <p>
                                            <?if (strlen($arDelivery["DESCRIPTION"])>0):?>
                                                <?= htmlspecialcharsback($arDelivery["DESCRIPTION"]) ?>
                                                <br />
                                            <?endif;?>
                                        </p>
                                    </div>
                    </div>
                    <?
                }
            }
        }
        else if ($delivery_coincidence) 
        {
            foreach ($delivery_coincidence_array as $delivery_id => $arDelivery)
            {
				if (count($arDelivery["STORE"]) > 0)
					$clickHandler = "onClick = \"fShowStore('".$arDelivery["ID"]."','".$arParams["SHOW_STORES_IMAGES"]."','".$width."','".SITE_ID."')\";";
				else
					$clickHandler = "onClick = \"BX('ID_DELIVERY_ID_".$arDelivery["ID"]."').checked=true;submitForm();\"";
				?>
					<div class="row <?if ($arDelivery['CHECKED']=='Y') echo ' active';?>" <?=$clickHandler?>>

							<input type="radio"
								id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
								name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
								value="<?= $arDelivery["ID"] ?>"<?if ($arDelivery["CHECKED"]=="Y") echo " checked";?>
								onclick="submitForm();"
								/>

								<?
								if (count($arDelivery["LOGOTIP"]) > 0):

									$arFileTmp = CFile::ResizeImageGet(
										$arDelivery["LOGOTIP"]["ID"],
										array("width" => "95", "height" =>"55"),
										BX_RESIZE_IMAGE_PROPORTIONAL,
										true
									);

									$deliveryImgURL = $arFileTmp["src"];
								else:
									$deliveryImgURL = $templateFolder."/images/logo-default-d.gif";
								endif;
								?>

								<div class="columns large-2 delivery-img">
									<img src="<?=$deliveryImgURL?>"/>
								</div>

								<div class="columns large-10">
									<strong>
									    <span class="bx_result_price">
											<?
											if (strlen($arDelivery["PERIOD_TEXT"])>0)
											{
												echo $arDelivery["PERIOD_TEXT"];
												?><br /><?
											}
											?>
                                                <?if($arDelivery["PRICE"] == 0):?>
                                                    <?=GetMessage("SALE_DELIV_FREE");?>
                                                <?else:?>
                                                    <?=GetMessage("SALE_DELIV_PRICE");?> <?=$arDelivery["PRICE_FORMATED"]?><br />
                                                <?endif;?>
										</span>
										<div class="name"><?= htmlspecialcharsbx($arDelivery["NAME"]) ?></div>
									</strong>
									
									<p>
                                            <?if (strlen($arDelivery["DESCRIPTION"])>0):?>
                                                <?= htmlspecialcharsback($arDelivery["DESCRIPTION"]) ?>
                                                <br />
                                            <?endif;?>
									</p>
								</div>
				</div>
				<?
            }
        }
		?>
		<?
	}
/*********** end delivery ***********/ ?>
<div class="clear"></div>
</div>
    
            
<?
    if (count($arResult["PAY_SYSTEM"]) > 1) {
        include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
    }
?>

	<h4>
		<?=GetMessage("SOA_TEMPL_BUYER_INFO")?>
		<?
		if ($bHideProps && $_POST["showProps"] != "Y"):
		?>
			<a href="#" class="slide" onclick="fGetBuyerProps(this); return false;">
				<?=GetMessage('SOA_TEMPL_BUYER_SHOW');?>
			</a>
		<?
		elseif ($bHideProps && $_POST["showProps"] == "Y"):
		?>
			<a href="#" class="slide" onclick="fGetBuyerProps(this); return false;">
				<?=GetMessage('SOA_TEMPL_BUYER_HIDE');?>
			</a>
		<?
		endif;
		?>
		<input type="hidden" name="showProps" id="showProps" value="N" />
	</h4>

	<div id="sale_order_props" <?=($bHideProps && $_POST["showProps"] != "Y")?"style='display:none;'":''?>>
		<?
		PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"], $arResult["USER_VALS"]["ORDER_PROP"], $arResult["ORDER_PROP"]["PRINT"]);
        ?>
	</div>
</div>

<?$style = (is_array($arResult["ORDER_PROP"]["RELATED"]) && count($arResult["ORDER_PROP"]["RELATED"])) ? "" : "display:none";
?>
<div class="bx_section" style="<?=$style?>">
	<?=PrintPropsForm($arResult["ORDER_PROP"]["RELATED"], $arParams["TEMPLATE_LOCATION"], $arResult["USER_VALS"]["ORDER_PROP"], $arResult["ORDER_PROP"]["PRINT"])?>
</div>

<script type="text/javascript">
	function fGetBuyerProps(el)
	{
		var show = '<?=GetMessageJS('SOA_TEMPL_BUYER_SHOW')?>';
		var hide = '<?=GetMessageJS('SOA_TEMPL_BUYER_HIDE')?>';
		var status = BX('sale_order_props').style.display;
		var startVal = 0;
		var startHeight = 0;
		var endVal = 0;
		var endHeight = 0;
		var pFormCont = BX('sale_order_props');
		pFormCont.style.display = "block";
		pFormCont.style.overflow = "hidden";
		pFormCont.style.height = 0;
		var display = "";

		if (status == 'none')
		{
			el.text = '<?=GetMessageJS('SOA_TEMPL_BUYER_HIDE');?>';

			startVal = 0;
			startHeight = 0;
			endVal = 100;
			endHeight = pFormCont.scrollHeight;
			display = 'block';
			BX('showProps').value = "Y";
			el.innerHTML = hide;
		}
		else
		{
			el.text = '<?=GetMessageJS('SOA_TEMPL_BUYER_SHOW');?>';

			startVal = 100;
			startHeight = pFormCont.scrollHeight;
			endVal = 0;
			endHeight = 0;
			display = 'none';
			BX('showProps').value = "N";
			pFormCont.style.height = startHeight+'px';
			el.innerHTML = show;
		}

		(new BX.easing({
			duration : 700,
			start : { opacity : startVal, height : startHeight},
			finish : { opacity: endVal, height : endHeight},
			transition : BX.easing.makeEaseOut(BX.easing.transitions.quart),
			step : function(state){
				pFormCont.style.height = state.height + "px";
				pFormCont.style.opacity = state.opacity / 100;
			},
			complete : function(){
					BX('sale_order_props').style.display = display;
					BX('sale_order_props').style.height = '';
			}
		})).animate();
	}
</script>