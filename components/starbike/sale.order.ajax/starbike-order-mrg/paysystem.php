<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="section">
<script type="text/javascript">
	function changePaySystem(param)
	{
		if (BX("account_only") && BX("account_only").value == 'Y') // PAY_CURRENT_ACCOUNT checkbox should act as radio
		{
			if (param == 'account')
			{
				if (BX("PAY_CURRENT_ACCOUNT"))
				{
					BX("PAY_CURRENT_ACCOUNT").checked = true;
					BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
					BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');

					// deselect all other
					var el = document.getElementsByName("PAY_SYSTEM_ID");
					for(var i=0; i<el.length; i++)
						el[i].checked = false;
				}
			}
			else
			{
				BX("PAY_CURRENT_ACCOUNT").checked = false;
				BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
				BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
			}
		}
		else if (BX("account_only") && BX("account_only").value == 'N')
		{
			if (param == 'account')
			{
				if (BX("PAY_CURRENT_ACCOUNT"))
				{
					BX("PAY_CURRENT_ACCOUNT").checked = !BX("PAY_CURRENT_ACCOUNT").checked;

					if (BX("PAY_CURRENT_ACCOUNT").checked)
					{
						BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
						BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
					}
					else
					{
						BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
						BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
					}
				}
			}
		}

		submitForm();
	}
</script>
<div class="bx_section">
	<h4><?=GetMessage("SOA_TEMPL_PAY_SYSTEM")?></h4>
	<?
	if ($arResult["PAY_FROM_ACCOUNT"] == "Y")
	{
		$accountOnly = ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y") ? "Y" : "N";
		?>
		<input type="hidden" id="account_only" value="<?=$accountOnly?>" />

		<div class="bx_block w100 vertical">
			<label class="bx_element<?if($arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y") echo " selected"?><?if ($arPaySystem['CHECKED']=='Y') echo ' active';?>"  id="PAY_CURRENT_ACCOUNT_LABEL" onclick="changePaySystem('account');">

				<input type="hidden" name="PAY_CURRENT_ACCOUNT" value="N">

					<input type="checkbox" name="PAY_CURRENT_ACCOUNT" id="PAY_CURRENT_ACCOUNT" value="Y"<?if($arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y") echo " checked=\"checked\"";?>>

					<div class="bx_logotype">
						<span style="background-image:url(<?=$templateFolder?>/images/logo-default-ps.gif);"></span>
					</div>

					<div class="bx_description">
						<strong><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT")?></strong>
						<p>
							<div><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT1")." <b>".$arResult["CURRENT_BUDGET_FORMATED"]?></b></div>
							<? if ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y"): ?>
								<div><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT3")?></div>
							<? else: ?>
								<div><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT2")?></div>
							<? endif; ?>
						</p>
					</div>
				<div class="clear"></div>
			</label>
		</div>
		<?
	}

	// first - those which have descriptions OR payment system has price
	foreach($arResult["PAY_SYSTEM"] as $arPaySystem)
	{
		if (strlen(trim(str_replace("<br />", "", $arPaySystem["DESCRIPTION"]))) > 0 || intval($arPaySystem["PRICE"]) > 0)
		{
			if (count($arResult["PAY_SYSTEM"]) == 1)
			{
				?>
				<div class="bx_block w100 vertical">
					<label class="bx_element<?if ($arPaySystem['CHECKED']=='Y') echo ' active';?>" onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">

						<input type="hidden" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>">

						<input type="radio"
							id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
							name="PAY_SYSTEM_ID"
							value="<?= $arPaySystem["ID"] ?>"
							<?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
							onclick="changePaySystem();"
							/>

							<?
							if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
								$imgUrl = $arPaySystem["PSA_LOGOTIP"]["SRC"];
							else:
								$imgUrl = $templateFolder."/images/logo-default-ps.gif";
							endif;
							?>
							<div class="bx_logotype">
								<span style="background-image:url(<?=$imgUrl?>);"></span>
							</div>

							<div class="bx_description">
								<?if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"):?>
									<strong><?=$arPaySystem["PSA_NAME"];?></strong>
								<?endif;?>
								<p>
									<?
									if (intval($arPaySystem["PRICE"]) > 0)
										echo str_replace("#PAYSYSTEM_PRICE#", SaleFormatCurrency(roundEx($arPaySystem["PRICE"], SALE_VALUE_PRECISION), $arResult["BASE_LANG_CURRENCY"]), GetMessage("SOA_TEMPL_PAYSYSTEM_PRICE"));
									else
										echo $arPaySystem["DESCRIPTION"];
									?>
								</p>
							</div>

						<div class="clear"></div>
					</label>
				</div>
				<?
			}
			else
			{
			?>
				<div class="bx_block w100 vertical">
					<label class="bx_element<?if ($arPaySystem['CHECKED']=='Y') echo ' active';?>" onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">

						<input type="radio"
							id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
							name="PAY_SYSTEM_ID"
							value="<?= $arPaySystem["ID"] ?>"
							<?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
							onclick="changePaySystem();" />

							<?
							if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
								$imgUrl = $arPaySystem["PSA_LOGOTIP"]["SRC"];
							else:
								$imgUrl = $templateFolder."/images/logo-default-ps.gif";
							endif;
							?>

							<div class="bx_logotype">
								<span style='background-image:url(<?=$imgUrl?>);'></span>
							</div>

							<div class="bx_description">
								<?if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"):?>
									<strong><?=$arPaySystem["PSA_NAME"];?></strong>
								<?endif;?>
								<p>
									<?
									if (intval($arPaySystem["PRICE"]) > 0)
										echo str_replace("#PAYSYSTEM_PRICE#", SaleFormatCurrency(roundEx($arPaySystem["PRICE"], SALE_VALUE_PRECISION), $arResult["BASE_LANG_CURRENCY"]), GetMessage("SOA_TEMPL_PAYSYSTEM_PRICE"));
									else
										echo $arPaySystem["DESCRIPTION"];
									?>
								</p>
							</div>

						<div class="clear"></div>
					</label>
				</div>
			<?
			}
		}
	}

	// payment system without descriptions OR payment_forward_calc
	foreach($arResult["PAY_SYSTEM"] as $arPaySystem)
	{
		if (strlen(trim(str_replace("<br />", "", $arPaySystem["DESCRIPTION"]))) == 0 && intval($arPaySystem["PRICE"]) == 0)
		{
			if (count($arResult["PAY_SYSTEM"]) == 1)
			{
				?>
				<div class="bx_block horizontal">
					<label class="bx_element<?if ($arPaySystem['CHECKED']=='Y') echo ' active';?>" onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">

						<input type="hidden" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>">

						<input type="radio"
							id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
							name="PAY_SYSTEM_ID"
							value="<?= $arPaySystem["ID"] ?>"
							<?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
							onclick="changePaySystem();"
							/>

						<?
						if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
							$imgUrl = $arPaySystem["PSA_LOGOTIP"]["SRC"];
						else:
							$imgUrl = $templateFolder."/images/logo-default-ps.gif";
						endif;
						?>

						<div class="bx_logotype">
							<span style='background-image:url(<?=$imgUrl?>);'></span>
						</div>

						<?if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"):?>
							<div class="bx_description">
								<div class="clear"></div>
								<strong><?=$arPaySystem["PSA_NAME"];?></strong>
							</div>
						<?endif;?>

					</label>
				</div>
			<?
			}
			else
			{
			?>
				<div class="bx_block horizontal">
					<label class="bx_element<?if ($arPaySystem['CHECKED']=='Y') echo ' active';?>" onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">

						<input type="radio"
							id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
							name="PAY_SYSTEM_ID"
							value="<?= $arPaySystem["ID"] ?>"
							<?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
							onclick="changePaySystem();" />

							<?
							if (count($arPaySystem["PSA_LOGOTIP"]) > 0):
								$imgUrl = $arPaySystem["PSA_LOGOTIP"]["SRC"];
							else:
								$imgUrl = $templateFolder."/images/logo-default-ps.gif";
							endif;
							?>

							<div class="bx_logotype">
								<span style='background-image:url(<?=$imgUrl?>);'></span>
							</div>

							<?if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"):?>
								<div class="bx_description">
									<div class="clear"></div>
									<strong>
										<?if ($arParams["SHOW_PAYMENT_SERVICES_NAMES"] != "N"):?>
											<?=$arPaySystem["PSA_NAME"];?>
										<?else:?>
											<?="&nbsp;"?>
										<?endif;?>
									</strong>
								</div>
							<?endif;?>
					</div>
				</label>
			<?
			}
		}
	}
	?>
	<div style="clear: both;"></div>
	</div>
</div>