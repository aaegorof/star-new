<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"]) > 0): ?>
<div class="about-us__companies">
<?
foreach($arResult["ITEMS"] as $key => $arElement):
		if(is_array($arElement)):
			$bPicture = is_array($arElement["PICTURE_PREVIEW"]);
		?>
			<div class="company-item">
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>">
					<?if ($bPicture):?>
						<img border="0" src="<?=$arElement["PICTURE_PREVIEW"]["SRC"]?>" width="105" height="70" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" />
					<?else:?>
						<img border="0" src="<?=SITE_TEMPLATE_PATH?>/images/no_images.png" width="105" height="70" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" />
					<?endif;?>
				</a>	
			</div>
		<?endif;?>
<?endforeach;?>
</div>
<?endif;?>