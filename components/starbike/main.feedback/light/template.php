<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>

<div class="feedback">
<h3>�������� �����</h3>

<div class="inputs_fb">							
<form action="<?=$APPLICATION->GetCurPage()?>" method="POST">
<?=bitrix_sessid_post()?>

<?if(!empty($arResult["ERROR_MESSAGE"]))
{
?>
<div style="color:red;">
<?
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
?>
</div>
<?
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div style="color:green;"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>

	<div class="left_input">
	
		<div class="input_fb">
			<label>���<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?> <span class="bx_sof_req">*</span><?endif?></label>
			<input type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>" size="40">
		</div>
		
		<div class="input_fb">
			<label>E-mail<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?> <span class="bx_sof_req">*</span><?endif?></label>
			<input type="text" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>" size="40">
		</div>
		
		<div class="input_fb">
			<label>�������</label>
				<input type="text" name="user_phone" size="40" />
		</div>

		<div class="textarea_fb">
			<label>���� ���������<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?> <span class="bx_sof_req">*</span><?endif?></label>
			<textarea name="MESSAGE" ><?=$arResult["MESSAGE"]?></textarea>
		</div>

		<?if($arParams["USE_CAPTCHA"] == "Y"):?>
			<div class="capcha">
				<label>���</label>
				<div class="code">
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
				</div>
				<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
				<input type="text" name="captcha_word" maxlength="50" value="" size="13">
			</div>
		<?endif;?>

		<div class="submit2">
			<input type="submit" name="submit" value="���������"/>
		</div>

	</div>

</form>
</div>
</div>