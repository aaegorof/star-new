<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>

<div class="main-feedback feedback">
<h3>�������� �����</h3>

<div class="inputs_fb">							
<form action="<?=$APPLICATION->GetCurPage()?>" method="POST" name="main_feedback_form">
<?=bitrix_sessid_post()?>

<?if(!empty($arResult["ERROR_MESSAGE"]))
{
?>
<div style="color:red;">
<?
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
?>
</div>
<?
}
?>
<?if(strlen($arResult["OK_MESSAGE"]) > 0):?>
<!--noindex--><a class="thank-you-fancybox" style="display:none;" href="#thank-you">��� ������ ���������</a><!--/noindex-->
            <script type="text/javascript">
                $(document).ready(function() {
                    $(".thank-you-fancybox").fancybox({
                            'transitionIn': 'none',
                            'transitionOut': 'none',
                            'autoDimensions': false,
                            'width': 580,
                            'height': 100,
                            'autoHeight': false,
                            'fitToView': true,
                            'scrolling': 'no',
                            'overlayShow': true,
                            'overlayColor': '#666',
                            'cyclic': true,
                            'titlePosition': 'over',
							'preload': true,
							'helpers': {
								'overlay': {
									'locked': false
								}
							}
                    });
                });
            </script>
<div id="thank-you" class="fancyboxform" style="text-align: center;"><font style="font-size: 32px; font-weight: bold;">���� ��������� ����������</font><br><br><strong>�������!</strong></div>
<?endif;?>

<?if(strlen($arResult["OK_MESSAGE"]) > 0):?>
<script>
$(function() {
    $(".thank-you-fancybox").click();
});
</script>
<?endif;?>

	<div class="left_input">
	
		<div class="input_fb">
			<label>���<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?> <span class="bx_sof_req">*</span><?endif?></label>
			<input type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>" size="40"<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?> required<?endif?>>
		</div>
		
		<div class="input_fb">
			<label>E-mail<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?> <span class="bx_sof_req">*</span><?endif?></label>
			<input type="email" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>" size="40"<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?> required<?endif?>>
		</div>
		
		<div class="input_fb">
			<label>�������</label>
				<input type="text" name="user_phone" size="40" />
		</div>

		<div class="textarea_fb">
			<label>���� ���������<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?> <span class="bx_sof_req">*</span><?endif?></label>
			<textarea name="MESSAGE"<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?> required<?endif?>><?=$arResult["MESSAGE"]?></textarea>
		</div>

		<?if($arParams["USE_CAPTCHA"] == "Y"):?>
			<div class="capcha">
				<label>���</label>
				<div class="code">
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
				</div>
				<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
				<input type="text" name="captcha_word" maxlength="50" value="" size="13">
			</div>
		<?endif;?>

		<div class="submit2">
			<input type="submit" class="web_form_submit" name="submit" value="���������"/>
		</div>
<small><font color="red"><span class="form-required starrequired">*</span></font> - ������������ ����</small>
	</div>

</form>
</div>
</div>
<script type="text/javascript">
    $(function(){

    /*jQuery.validate*/
    var mainFeedbackForm = $('form[name="main_feedback_form"]');
    mainFeedbackForm.validate({
        focusInvalid: false,
        focusCleanup: true,
        submitHandler: function(form) {
            form.submit();
        },
		  errorPlacement: function(error, element) {
		  }
    });

  })
</script>