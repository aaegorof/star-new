<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if (count($arResult["ITEMS"]) < 1)
	return;
	
$step = 0;
?>
<div class="news">
	<div class="title"><a href="/news">�������</a></div>
	<div class="news-items">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<?$step++;?>
		<div class="news-item<?if($step != count($arResult["ITEMS"])){?> margin<?}?>">
			<div class="news-item__img"><?if($arItem["PREVIEW_PICTURE"]["SRC"]):?><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" width="50" /><?endif;?></div>
			<div class="news-item__desc">
				<div class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
				<div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
			</div>
		</div>
	<?endforeach;?>
	</div>
</div>
