<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalCss("/bitrix/css/main/font-awesome.css");
?>
<?if (count($arResult["ITEMS"]) > 0):?>
<div class="bx-newslist">
<div class="row">

	<div class="bx-newslist-container col-xs-12" id="slider-container">
	<div class="bx-newslist-block">
	<?if (count($arResult["ITEMS"]) > 1):?>
				<div class="bx-newslist-slider">
					<div class="bx-newslist-slider-container" style="width: <?echo count($arResult["ITEMS"])*100?>%; left: 0;">
						<?foreach($arResult["ITEMS"] as $arItem):?>
						<div style="width: <?echo 100/count($arResult["ITEMS"])?>%;" class="bx-newslist-slider-slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<?if($arItem["PROPERTIES"]["LINK"]["VALUE"]):?>
							<a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>">
								<img
									src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
									width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
									height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
									alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
									title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
								/>
							</a>
						<?else:?>
							<img
								src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
								width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
								height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
								alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
								title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
							/>
						<?endif;?>
						</div>
						<?endforeach;?>
						<div style="clear: both;"></div>
					</div>
					<div class="bx-newslist-slider-arrow-container-left"><div class="bx-newslist-slider-arrow"><i class="fa fa-angle-left" ></i></div></div>
					<div class="bx-newslist-slider-arrow-container-right"><div class="bx-newslist-slider-arrow"><i class="fa fa-angle-right"></i></div></div>
				</div>
					<ul class="bx-newslist-slider-control">
						<?foreach ($arResult["ITEMS"] as $i => $file):?>
							<li rel="<?=($i+1)?>" <?if (!$i) echo 'class="current"'?>><span></span></li>
						<?endforeach?>
					</ul>
				<script type="text/javascript">
					BX.ready(function() {
						new JCNewsSlider('slider-container', {
							imagesContainerClassName: 'bx-newslist-slider-container',
							leftArrowClassName: 'bx-newslist-slider-arrow-container-left',
							rightArrowClassName: 'bx-newslist-slider-arrow-container-right',
							controlContainerClassName: 'bx-newslist-slider-control'
						});
						var slidesAmount = $(".bx-newslist-slider-control li").length;
						var i = 1;
						var timerId = setTimeout(function run() {
							var timerId1;
							var timerId2;
							$('.bx-newslist-slider').hover(function(ev){
								clearTimeout(timerId1);
							}, function(ev){
								setTimeout(run, 6000);
							});
							if (slidesAmount != i) {
								$('.bx-newslist-slider-arrow-container-right').click();
								timerId1 = setTimeout(run, 6000);
								i++;
							}
							else {
								clearTimeout(timerId);
							}
						}, 6000);

					});
				</script>
	<?else:?>
		<div class="bx-newslist-img">
		<?if($arResult["ITEMS"][0]["PROPERTIES"]["LINK"]["VALUE"]):?>
			<a href="<?=$arResult["ITEMS"][0]["PROPERTIES"]["LINK"]["VALUE"]?>">
				<img
					src="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["SRC"]?>"
					width="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["WIDTH"]?>"
					height="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["HEIGHT"]?>"
					alt="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["TITLE"]?>"
				/>
			</a>
		<?else:?>
			<img
				src="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["SRC"]?>"
				width="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["WIDTH"]?>"
				height="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["HEIGHT"]?>"
				alt="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["ALT"]?>"
				title="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["TITLE"]?>"
				/>
		<?endif;?>
		</div>
	<?endif;?>
	</div>
	</div>
</div>
</div>
<?endif;?>