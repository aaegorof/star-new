<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalCss("/bitrix/css/main/font-awesome.css");
?>
<?if (count($arResult["ITEMS"]) > 0):?>
<div class="banners">
	<?if (count($arResult["ITEMS"]) > 1):?>
						<?foreach($arResult["ITEMS"] as $arItem):?>
						<div class="slide-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<?if(!empty($arItem["PROPERTIES"]["LINK"]["VALUE"])):?>
								<div class="slide-item__img">
										<a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>"><img
											src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
											width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
											height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
											alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
											title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
									/></a>
								</div>

								<?else:?>
								<div class="slide-item__img">
									<img
										src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
										width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
										height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
										alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
										title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
									/>
								</div>
							<?endif;?>
						</div>
						<?endforeach;?>

	<?else:?>
		<div class="bx-newslist-img">
		<?if($arResult["ITEMS"][0]["PROPERTIES"]["LINK"]["VALUE"]):?>
			<a href="<?=$arResult["ITEMS"][0]["PROPERTIES"]["LINK"]["VALUE"]?>">
				<img
					src="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["SRC"]?>"
					width="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["WIDTH"]?>"
					height="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["HEIGHT"]?>"
					alt="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["TITLE"]?>"
				/>
			</a>
		<?else:?>
			<img
				src="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["SRC"]?>"
				width="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["WIDTH"]?>"
				height="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["HEIGHT"]?>"
				alt="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["ALT"]?>"
				title="<?=$arResult["ITEMS"][0]["PREVIEW_PICTURE"]["TITLE"]?>"
				/>
		<?endif;?>
		</div>
	<?endif;?>
</div>
<?endif;?>