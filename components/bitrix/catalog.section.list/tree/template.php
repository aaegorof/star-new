<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strTitle = "";
?>

	<?
	$TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
	$CURRENT_DEPTH = $TOP_DEPTH;
$j = 1;
	foreach($arResult["SECTIONS"] as $arSection)
	{
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
		if($CURRENT_DEPTH < $arSection["DEPTH_LEVEL"])
		{
			switch ($arSection["DEPTH_LEVEL"]):
				case 1:
					echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH), '<ul class="active" data-menu="1">';
					break;
				case 2:
					echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH), '<ul class="submenu_catalog">';
					break;
				case 3:
					echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH), '<ul class="sub_level-ul">';
					break;
				case 4:
					echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH), '<ul class="sub_level-ul">';
					break;
				case 5:
					echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH), '<ul class="sub_level-ul">';
					break;
				case 6:
					echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH), '<ul class="sub_level-ul">';
					break;
				default:
					echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH), '<ul class="sub_level-ul">';
			endswitch;
		}
		elseif($CURRENT_DEPTH == $arSection["DEPTH_LEVEL"])
		{
			echo "</li>";
		}
		else
		{
			while($CURRENT_DEPTH > $arSection["DEPTH_LEVEL"])
			{
				echo "</li>";
				echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul><div class='li-expand'></div>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
				$CURRENT_DEPTH--;
			}
			echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),'</li>';
		}

			if(($arSection["DEPTH_LEVEL"] > 2) && ($arSection["DEPTH_LEVEL"] < $arResult["SECTIONS"][$j]["DEPTH_LEVEL"])) {
				$link = '<a href="'.$arSection["SECTION_PAGE_URL"].'" style="position: relative;">'.$arSection["NAME"].'</a>';
			} elseif($arSection["DEPTH_LEVEL"] >= 2) {
				$link = '<a href="'.$arSection["SECTION_PAGE_URL"].'">'.$arSection["NAME"].'</a>';
			} elseif($arSection["DEPTH_LEVEL"] == 1) {
				$link = '<a href="'.$arSection["SECTION_PAGE_URL"].'" class="opener"><span>'.$arSection["NAME"].'</span></a>';
			} else {
				$link = 'W';
			}

		echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH);
		?>

		<li id="<?=$this->GetEditAreaId($arSection['ID']);?>" class="sub_level-li"><?=$link?>

		<?$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
	$j++;
	}

	while($CURRENT_DEPTH > $TOP_DEPTH)
	{
		echo "</li>";
		echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
		$CURRENT_DEPTH--;


	}
	?>
<?=($strTitle?'<br/><h2>'.$strTitle.'</h2>':'')?>
