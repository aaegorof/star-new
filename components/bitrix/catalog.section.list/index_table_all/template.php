<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$SECTIONS = array();
#trace($arResult["SECTIONS"]);
$CURRENT_DEPTH=$arResult["SECTION"]["DEPTH_LEVEL"]+1;
foreach($arResult["SECTIONS"] as $arSection):
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));

	$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];

	$COUNT_SECTION = CIBlockSection::GetCount(Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "SECTION_ID"=>$arSection["ID"]));
	if($COUNT_SECTION == 0) continue;
	?>
	<h2 class="first-section-h"><?=$arSection["NAME"];?></h2>
	<div class="first-sections">
	<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "index_table", array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "3",
		"SECTION_ID" => $arSection["ID"],
		"SECTION_CODE" => $arSection["CODE"],
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "3",
		"SECTION_FIELDS" => array(
			0 => "PICTURE",
			1 => "",
		),
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_URL" => "",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "36000",
		"CACHE_GROUPS" => "Y",
		"ADD_SECTIONS_CHAIN" => "N"
		),
		false
	);?>
	</div>
	<?
endforeach;
?>
