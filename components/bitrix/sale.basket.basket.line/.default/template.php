<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
require_once($_SERVER["DOCUMENT_ROOT"]."/include/application.php");
#trace($arResult);
CModule::IncludeModule('iblock');
CModule::IncludeModule('sale');
$ALL_PRICE = $ALL_QUANTITY = 0;

// ������� ���������� ������� ��� �������� ������������
$arBasketItems = array();
$dbBasketItems = CSaleBasket::GetList(
        array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
        array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
        false,
        false,
        array("ID", "CALLBACK_FUNC", "MODULE", 
              "PRODUCT_ID", "QUANTITY", "DELAY", 
              "CAN_BUY", "PRICE", "WEIGHT", "NAME")
    );
while ($arItems = $dbBasketItems->Fetch())
{
    if (strlen($arItems["CALLBACK_FUNC"]) > 0)
    {
        CSaleBasket::UpdatePrice($arItems["ID"], 
                                 $arItems["CALLBACK_FUNC"], 
                                 $arItems["MODULE"], 
                                 $arItems["PRODUCT_ID"], 
                                 $arItems["QUANTITY"]);
        $arItems = CSaleBasket::GetByID($arItems["ID"]);
    }

    $arBasketItems[] = $arItems;
	
	$ALL_PRICE = $ALL_PRICE + ($arItems["PRICE"]*$arItems["QUANTITY"]);
	$ALL_QUANTITY++;
}?>
	<span id="cart_line">
		<a class="b_h1" href="<?=$arParams["PATH_TO_BASKET"]?>" id="cart-status">
			<div class="fa fa-shopping-cart">
			</div>
			<div class="counter-products-wrap">
				<div class="counter-products">
					<span class="counter-product__numbers"><?=$ALL_QUANTITY;?></span>
				</div>
				<div class="sum-products">
					�������
					<br> �� ����� <?=number_format($ALL_PRICE,0,'',' ')?> ���.
				</div>
			</div>
		</a>
	</span>