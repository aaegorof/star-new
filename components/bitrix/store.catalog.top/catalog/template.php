<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
foreach($arResult["ROWS"] as $arItems):
	foreach($arItems as $key => $arElement):

		if(is_array($arElement)):
			$bPicture = is_array($arElement["PREVIEW_IMG"]);
?>
		<div class="item_rs">
			<div class="border_rs">
				<?if ($bPicture):?>
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>" width="110" /></a>
				<?else:?>
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/no_images.png" width="110" /></a>
				<?endif;?>
				<p style="height: auto;"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></p>
			</div>
		</div>
<?
		endif;
	endforeach;
endforeach;
?>

