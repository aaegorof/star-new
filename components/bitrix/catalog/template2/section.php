<div class="catalog">
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if($arParams["USE_COMPARE"]=="Y"):
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.compare.list",
		"store",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"NAME" => $arParams["COMPARE_NAME"],
			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
			"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
		),
		$component
	);
endif;
?>

<?$APPLICATION->IncludeComponent("api:search.filter", "search_filter", Array(
	"COMPONENT_TEMPLATE" => "gray-blue",
		"IBLOCK_TYPE" => "catalog",	// ��� ���������
		"IBLOCK_ID" => "3",	// ��������
		"FILTER_NAME" => "arrFilter",	// ��� ���������� ������� ��� ����������
		"REDIRECT_FOLDER" => "",	// ������� ��� �������� ������
		"FIELD_CODE" => array(	// ����
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(	// ��������
			0 => "BRAND",
			1 => "",
		),
		"CHECK_ACTIVE_SECTIONS" => "N",	// ��������� ���������� �������� ��� ����������
		"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID ������� �� $_REQUEST
		"SECTION_CODE" => $_REQUEST["SECTION_CODE"],	// ��� ������� �� $_REQUEST
		"LIST_HEIGHT" => "5",	// ������ ������� �������������� ������, (�����)
		"TEXT_WIDTH" => "209",	// ������ ������������ ��������� �����, (px)
		"NUMBER_WIDTH" => "85",	// ������ ����� ����� ��� �������� ����������, (px)
		"SELECT_WIDTH" => "220",	// ������ ����� ���������� �������, (px)
		"ELEMENT_IN_ROW" => "3",	// ����� � ����� ������, (��)
		"NAME_WIDTH" => "130",	// ������ �������� �����, (px)
		"FILTER_TITLE" => "������",	// ��������� �������
		"BUTTON_ALIGN" => "left",	// ������������ ������
		"SELECT_IN_CHECKBOX" => array(	// ���������� ������ �������
			0 => "BRAND",
			1 => "",
		),
		"CHECKBOX_NEW_STRING" => "N",	// ������ � ����� ������
		"REPLACE_ALL_LABEL" => "N",	// �������� (���) �� �������� ���� �������
		"REMOVE_POINTS" => "N",	// ������ ����� �� ������� � ����� ��������
		"SECTIONS_DEPTH_LEVEL" => "",	// �������� �������� ������ ��������
		"SECTIONS_FIELD_TITLE" => "�������",	// �������� ������� c ���������
		"SECTIONS_FIELD_VALUE_TITLE" => "��� �������",	// �������� ��������� ������� �������
		"CACHE_TYPE" => "A",	// ��� �����������
		"CACHE_TIME" => "36000000",	// ����� ����������� (���.)
		"CACHE_GROUPS" => "Y",	// ��������� ����� �������
		"SAVE_IN_SESSION" => "N",	// ��������� ��������� ������� � ������ ������������
		"PRICE_CODE" => array(	// ��� ����
			0 => "NEW",
		),
		"INCLUDE_JQUERY" => "Y",	// �������� jQuery
		"INCLUDE_PLACEHOLDER" => "Y",	// �������� placeholder
		"INCLUDE_CHOSEN_PLUGIN" => "Y",	// �������� ���������� ���������� �������
		"CHOSEN_PLUGIN_PARAM__disable_search_threshold" => "30",	// ����� ��������� ������ �� ���������
		"INCLUDE_FORMSTYLER_PLUGIN" => "Y",	// �������� ���������� �������/��������������
		"INCLUDE_AUTOCOMPLETE_PLUGIN" => "N",	// �������� �����-����������� ��� ��������� �����
		"INCLUDE_JQUERY_UI" => "Y",	// �������� jQuery UI
		"INCLUDE_JQUERY_UI_SLIDER" => "Y",	// �������� ��������
		"JQUERY_UI_SLIDER_BORDER_RADIUS" => "Y",	// ������� ��������
		"INCLUDE_JQUERY_UI_SLIDER_TOOLTIP" => "Y",	// �������� ������
		"JQUERY_UI_THEME" => "excite-bike",	// ���� jQuery UI
		"JQUERY_UI_FONT_SIZE" => "10px",	// ������ ������ jQuery UI
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"list",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"TOP_DEPTH" => 6,
		"ACTIVE_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"DISPLAY_IMG_WIDTH"	 =>	$arParams["DISPLAY_IMG_WIDTH"],
		"DISPLAY_IMG_HEIGHT" =>	$arParams["DISPLAY_IMG_HEIGHT"],
		"SHARPEN" =>	$arParams["SHARPEN"],
	),
	$component
);
?>

<!--noindex-->
<?/*if($arParams["USE_FILTER"]=="Y"):?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:store.catalog.filter",
		"",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"FILTER_NAME" => '',
			"FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
			"PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
			"PRICE_CODE" => $arParams["FILTER_PRICE_CODE"],
			"OFFERS_FIELD_CODE" => $arParams["FILTER_OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => $arParams["FILTER_OFFERS_PROPERTY_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		),
		$component
	);
	?>
<?endif*/?>
	<?$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "smart_filter", Array(
	"IBLOCK_TYPE" => "catalog",	// ��� ���������
		"IBLOCK_ID" => "3",	// ��������
		"SECTION_ID" => "",	// ID ������� ���������
		"FILTER_NAME" => "arrFilter",	// ��� ���������� ������� ��� ����������
		"PRICE_CODE" => "",	// ��� ����
		"CACHE_TYPE" => "A",	// ��� �����������
		"CACHE_TIME" => "36000000",	// ����� ����������� (���.)
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",	// ��������� ����� �������
		"SAVE_IN_SESSION" => "N",	// ��������� ��������� ������� � ������ ������������
		"COMPONENT_TEMPLATE" => "template1",
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],	// ��� �������
		"HIDE_NOT_AVAILABLE" => "N",	// �� ���������� ������, ������� ��� �� �������
		"TEMPLATE_THEME" => "",	// �������� ����
		"FILTER_VIEW_MODE" => "horizontal",	// ��� �����������
		"DISPLAY_ELEMENT_COUNT" => "N",	// ���������� ����������
		"SEF_MODE" => "N",	// �������� ��������� ���
		"INSTANT_RELOAD" => "N",	// ���������� ���������� ��� ���������� AJAX
		"PAGER_PARAMS_NAME" => "arrPager",	// ��� ������� � ����������� ��� ���������� ������ � ������������ ���������
		"CONVERT_CURRENCY" => "N",	// ���������� ���� � ����� ������
		"XML_EXPORT" => "N",	// �������� ��������� ������ ��������
		"SECTION_TITLE" => "-",	// ���������
		"SECTION_DESCRIPTION" => "-",	// ��������
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>

<?/*
$arAvailableSort = array(
	"sort" => Array("sort", "asc"), 
	"price" => Array('PROPERTY_PRICEHIDDEN', "asc"),
);

$sort = array_key_exists("sort", $_REQUEST) && array_key_exists(ToLower($_REQUEST["sort"]), $arAvailableSort) ? $arAvailableSort[ToLower($_REQUEST["sort"])][0] : "sort";
$sort_order = array_key_exists("order", $_REQUEST) && in_array(ToLower($_REQUEST["order"]), Array("asc", "desc")) ? ToLower($_REQUEST["order"]) : 'asc';	

*/?>
<?if ($_GET["sort"] == "name") {
        $arParams["ELEMENT_SORT_FIELD"] = 'NAME';
        $arParams["ELEMENT_SORT_ORDER"] = $_GET["order"];
} elseif ($_GET["sort"] == "price") {
        $arParams["ELEMENT_SORT_FIELD"] = 'PROPERTY_PRICEHIDDEN';
        $arParams["ELEMENT_SORT_ORDER"] = $_GET["order"];
} 
?>

<!--/noindex-->
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
		"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
		"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
 		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
		"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
		"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
		"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],

		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
	
		"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
		"COMPARE_NAME" => $arParams["COMPARE_NAME"],
		
		"DISPLAY_IMG_WIDTH"	 =>	$arParams["DISPLAY_IMG_WIDTH"],
		"DISPLAY_IMG_HEIGHT" =>	$arParams["DISPLAY_IMG_HEIGHT"],
	
		"SHARPEN" => $arParams["SHARPEN"],
		
		"ADD_SECTIONS_CHAIN" => "Y"
	),
	$component
);
?> 
</div>