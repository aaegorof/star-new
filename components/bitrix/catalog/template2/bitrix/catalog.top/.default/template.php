<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="section">

	<div class="section_content">

	<?foreach($arResult["ROWS"] as $arItems):?>
		<?foreach($arItems as $arElement):?>
			<?
			if($arElement["ID"]):
			$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CATALOG_ELEMENT_DELETE_CONFIRM')));

				$bHasPicture = is_array($arElement['PREVIEW_IMG']);

				$sticker = "";
				if (array_key_exists("PROPERTIES", $arElement) && is_array($arElement["PROPERTIES"]))
				{
					foreach (Array("SPECIALOFFER", "NEWPRODUCT", "SALELEADER") as $propertyCode)
						if (array_key_exists($propertyCode, $arElement["PROPERTIES"]) && intval($arElement["PROPERTIES"][$propertyCode]["PROPERTY_VALUE_ID"]) > 0)
							$sticker .= "&nbsp;<span class=\"sticker\">".$arElement["PROPERTIES"][$propertyCode]["NAME"]."</span>";
				}

			?>
					<div class="section_content_item">
						<div class="white_border">
							<?if($bHasPicture):?>
								<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>" width="<?=$arElement["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_IMG"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" /></a>
							<?endif;?>
							<div class="sc_text">
								<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a>
								<p><?=$arElement['PREVIEW_TEXT']?></p>
							</div>
							<div class="cost">
								<div class="cost_c">
									<?foreach($arElement["PRICES"] as $code=>$arPrice):
										if($arPrice["CAN_ACCESS"]):
									?>
										<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
											<p><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></p>
											<span><?=$arPrice["PRINT_VALUE"]?></span>
										<?else:?>
											<p><?=$arPrice["PRINT_VALUE"]?></p>
										<?endif;?>
									<?
										endif;
									endforeach;
									?>
								</div>
								<?if ($arElement['CAN_BUY']):?>
									<a href="<?echo $arElement["ADD_URL"]?>" class="cost_buy" rel="nofollow"  onclick="return addToCart(this, 'catalog_list_image_<?=$arElement['ID']?>', 'list', '<?=GetMessage("CATALOG_IN_CART")?>');" id="catalog_add2cart_link_<?=$arElement['ID']?>">������</a>
								<?elseif (count($arResult["PRICES"]) > 0):?>
									<span class="cost_buy"><?=GetMessage('CATALOG_NOT_AVAILABLE')?></span>
								<?endif;?>
							</div>
							<?if(is_array($arElement["PROPERTIES"]["RECOMMEND"]["VALUE"])):?>
								<div class="table_cost">
								<table>
								<?
								foreach($arElement["PROPERTIES"]["RECOMMEND"]["VALUE"] as $valRec):
									$resRecommend = CIBlockElement::GetByID($valRec)->GetNext();
									
									// ��������� ���� ����� ���������� ������ ~~~~~~~~~~~~
										$PriceCatalogProduct = CPrice::GetBasePrice($resRecommend["ID"]);
										$PriceProduct = $PriceDiscountProduct = $PriceCatalogProduct["PRICE"]; // ������� ���� ������
										
										$dbProductDiscounts = CCatalogDiscount::GetList(
													array("SORT" => "DESC"), 
													array("+PRODUCT_ID" => $arItem["ID"], "ACTIVE" => "Y", "!>ACTIVE_FROM" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")), "!<ACTIVE_TO" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")), "COUPON" => ""), false, false, 
													array("ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO", "RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE", "VALUE", "CURRENCY", "PRODUCT_ID")
											);
										while ($arProductDiscounts = $dbProductDiscounts->Fetch())
										{
											// ���� �� �������
											if($arProductDiscounts["VALUE_TYPE"] == "P")
												$PriceDiscountProduct = ceil($PriceProduct - ($PriceProduct * (IntVal($arProductDiscounts["VALUE"])/100))); 
											else
												$PriceDiscountProduct = $PriceProduct - $arProductDiscounts["VALUE"]; 
										}

										if($PriceProduct != $PriceDiscountProduct){// �������� ���� ������ � ������ ������
											$PriceProduct = $PriceDiscountProduct;
										}
									//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
								?>
									<tr>
										<td><p class="t_name" id="catalog_recomend_list_<?=$resRecommend['ID']?>"><?=$resRecommend["NAME"];?></p></td>
										<td><p class="t_price"><?=number_format($PriceProduct, 0, '', ' ');?> �.</p></td>
										<td><a href="/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=<?=$PriceCatalogProduct["ID"]?>&PRODUCT_ID=<?=$resRecommend["ID"]?>" class="t_buy" onclick="return addToCart(this, 'catalog_recomend_list_<?=$resRecommend['ID']?>', 'list', '������');">������</a></td>
									</tr>
								<?endforeach;?>
								</table>
								</div>
							<?endif;?>
							
						</div>
					</div>
				<?endif;?>
		<?endforeach?>
	<?endforeach?>
	</div>
</div>
