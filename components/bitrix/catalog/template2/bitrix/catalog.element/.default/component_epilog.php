<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

__IncludeLang($_SERVER["DOCUMENT_ROOT"].$templateFolder."/lang/".LANGUAGE_ID."/template.php");

//$APPLICATION->AddHeadScript('/bitrix/templates/'.SITE_TEMPLATE_ID.'/jquery/fancybox/jquery.fancybox-1.3.1.pack.js');
//$APPLICATION->SetAdditionalCSS('/bitrix/templates/'.SITE_TEMPLATE_ID.'/jquery/fancybox/jquery.fancybox-1.3.1.css');

if (CModule::IncludeModule('sale'))
{
	$dbBasketItems = CSaleBasket::GetList(
		array(
			"ID" => "ASC"
		),
		array(
			"PRODUCT_ID" => $arResult['ID'],
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL",
		),
		false,
		false,
		array()
	);

	if ($arBasket = $dbBasketItems->Fetch())
	{
		if($arBasket["DELAY"] == "Y")
			echo "<script type=\"text/javascript\">$(function() {disableAddToCart('catalog_add2cart_link', 'detail', '".GetMessage("CATALOG_IN_CART_DELAY")."')});</script>\r\n";
		else
			echo "<script type=\"text/javascript\">$(function() {disableAddToCart('catalog_add2cart_link', 'detail', '".GetMessage("CATALOG_IN_BASKET")."')});</script>\r\n";
	}
}

if ($arParams['USE_COMPARE'])
{
	if (isset(
		$_SESSION[$arParams["COMPARE_NAME"]][$arParams["IBLOCK_ID"]]["ITEMS"][$arResult['ID']]
	))
	{
		echo '<script type="text/javascript">$(function(){disableAddToCompare(\'catalog_add2compare_link\', \''.GetMessage("CATALOG_IN_COMPARE").'\');})</script>';
	}
}

if (array_key_exists("PROPERTIES", $arResult) && is_array($arResult["PROPERTIES"]))
{
	$sticker = "";

	foreach (Array("SPECIALOFFER", "NEWPRODUCT", "SALELEADER") as $propertyCode)
	{
		if (array_key_exists($propertyCode, $arResult["PROPERTIES"]) && intval($arResult["PROPERTIES"][$propertyCode]["PROPERTY_VALUE_ID"]) > 0)
			$sticker .= "&nbsp;<span class=\"sticker\">".$arResult["PROPERTIES"][$propertyCode]["NAME"]."</span>";
	}

	if ($sticker != "")
		$APPLICATION->SetPageProperty("ADDITIONAL_TITLE", $sticker);
}
if (count($arResult['OFFERS_IDS']) > 0 && CModule::IncludeModule('sale'))
{
	$arItemsInCompare = array();
	foreach ($arResult['OFFERS_IDS'] as $ID)
	{
		if (isset(
			$_SESSION[$arParams["COMPARE_NAME"]][$arParams["IBLOCK_ID"]]["ITEMS"][$ID]
		))
			$arItemsInCompare[] = $ID;
	}

	$dbBasketItems = CSaleBasket::GetList(
		array(
			"ID" => "ASC"
		),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL",
			),
		false,
		false,
		array()
	);

	$arPageItems = array();
	$arPageItemsDelay = array();
	while ($arItem = $dbBasketItems->Fetch())
	{
		if (in_array($arItem['PRODUCT_ID'], $arResult['OFFERS_IDS']))
		{
			if($arItem["DELAY"] == "Y")
				$arPageItemsDelay[] = $arItem['PRODUCT_ID'];
			else
				$arPageItems[] = $arItem['PRODUCT_ID'];
		}
	}

	if (count($arPageItems) > 0 || count($arPageItemsDelay) > 0)
	{
		echo '<script type="text/javascript">$(function(){'."\r\n";
		foreach ($arPageItems as $id) 
		{
			echo "disableAddToCart('catalog_add2cart_link_ofrs_".$id."', 'list', '".GetMessage("CATALOG_IN_BASKET")."');\r\n";
		}
		foreach ($arPageItemsDelay as $id) 
		{
			echo "disableAddToCart('catalog_add2cart_link_ofrs_".$id."', 'list', '".GetMessage("CATALOG_IN_CART_DELAY")."');\r\n";
		}
		echo '})</script>';
	}
	
	if (count($arItemsInCompare) > 0)
	{
		echo '<script type="text/javascript">$(function(){'."\r\n";
		foreach ($arItemsInCompare as $id) 
		{
			echo "disableAddToCompare('catalog_add2compare_link_ofrs_".$id."', '".GetMessage("CATALOG_IN_COMPARE")."');\r\n";
		}
		echo '})</script>';
	}
}
?>
<?if($_GET['formresult'] == 'addok'):?>
<!--noindex--><a class="thank-you-fancybox" style="display:none;" href="#thank-you">��� ������ ���������</a><!--/noindex-->
			<script type="text/javascript">
				$(document).ready(function() {
					$(".thank-you-fancybox").fancybox({
							'transitionIn': 'none',
							'transitionOut': 'none',
							'autoDimensions': false,
							'width': 580,
							'height': 100,
							'autoHeight': false,
							'fitToView': true,
							'scrolling': 'no',
							'overlayShow': true,
							'overlayColor': '#666',
							'cyclic': true,
							'titlePosition': 'over',
							'preload': true,
					});
				});
			</script>
<div id="thank-you" class="fancyboxform" style="text-align: center;"><font style="font-size: 32px; font-weight: bold;">��� ������ ���������</font><br><br><strong>�������!</strong></div>
<?endif;?>

<?if($_GET['formresult'] == 'addok'):?>
<script>
$(function() {
	$(".thank-you-fancybox").click();
});
</script>
<?unset($_GET);?>
<?endif;?>

<div id="form_1" class="fancyboxform" style="display:none;">
<?$APPLICATION->IncludeComponent(
		"starbike:form.result.new", 
		"fancyboxform_1", 
		array(
			"SEF_MODE" => "Y",
			"WEB_FORM_ID" => "1",
			"LIST_URL" => "",
			"EDIT_URL" => "",
			"SUCCESS_URL" => "",
			"CHAIN_ITEM_TEXT" => "",
			"CHAIN_ITEM_LINK" => "",
			"IGNORE_CUSTOM_TEMPLATE" => "Y",
			"USE_EXTENDED_ERRORS" => "Y",
			"CACHE_TYPE" => "N",
			"CACHE_TIME" => "3600",
			"SEF_FOLDER" => "/",
			"COMPONENT_TEMPLATE" => "fancyboxform_1"
		),
		$component
		);?>
</div>
<div id="form_3" class="fancyboxform" style="display:none;">
	<?$APPLICATION->IncludeComponent(
		"starbike:form.result.new", 
		"fancyboxform_3", 
		array(
			"SEF_MODE" => "Y",
			"WEB_FORM_ID" => "3",
			"LIST_URL" => "",
			"EDIT_URL" => "",
			"SUCCESS_URL" => "",
			"CHAIN_ITEM_TEXT" => "",
			"CHAIN_ITEM_LINK" => "",
			"IGNORE_CUSTOM_TEMPLATE" => "Y",
			"USE_EXTENDED_ERRORS" => "Y",
			"CACHE_TYPE" => "N",
			"CACHE_TIME" => "3600",
			"SEF_FOLDER" => "/",
			"COMPONENT_TEMPLATE" => "fancyboxform_3"
		),
		$component
);?>
</div>
<div id="form_2" class="fancyboxform" style="display:none;">
	<?$APPLICATION->IncludeComponent(
		"starbike:form.result.new", 
		"fancyboxform_2", 
		array(
			"SEF_MODE" => "Y",
			"WEB_FORM_ID" => "2",
			"LIST_URL" => "",
			"EDIT_URL" => "",
			"SUCCESS_URL" => "",
			"CHAIN_ITEM_TEXT" => "",
			"CHAIN_ITEM_LINK" => "",
			"IGNORE_CUSTOM_TEMPLATE" => "Y",
			"USE_EXTENDED_ERRORS" => "Y",
			"CACHE_TYPE" => "N",
			"CACHE_TIME" => "3600",
			"SEF_FOLDER" => "/",
			"COMPONENT_TEMPLATE" => "fancyboxform_2"
		),
		$component
	);?>
</div>