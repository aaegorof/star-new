<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
    <h1 class="title"><?= $arResult["NAME"] ?></h1>
    <!--[if IE]>
    <style type="text/css">
        #fancybox-loading.fancybox-ie div {
            background: transparent;
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_loading.png', sizingMethod='scale');
        }

        .fancybox-ie #fancybox-close {
            background: transparent;
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_close.png', sizingMethod='scale');
        }

        .fancybox-ie #fancybox-title-over {
            background: transparent;
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_over.png', sizingMethod='scale');
            zoom: 1;
        }

        .fancybox-ie #fancybox-title-left {
            background: transparent;
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_left.png', sizingMethod='scale');
        }

        .fancybox-ie #fancybox-title-main {
            background: transparent;
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_main.png', sizingMethod='scale');
        }

        .fancybox-ie #fancybox-title-right {
            background: transparent;
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_right.png', sizingMethod='scale');
        }

        .fancybox-ie #fancybox-left-ico {
            background: transparent;
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_nav_left.png', sizingMethod='scale');
        }

        .fancybox-ie #fancybox-right-ico {
            background: transparent;
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_nav_right.png', sizingMethod='scale');
        }

        .fancybox-ie .fancy-bg {
            background: transparent !important;
        }

        .fancybox-ie #fancy-bg-n {
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_n.png', sizingMethod='scale');
        }

        .fancybox-ie #fancy-bg-ne {
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_ne.png', sizingMethod='scale');
        }

        .fancybox-ie #fancy-bg-e {
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_e.png', sizingMethod='scale');
        }

        .fancybox-ie #fancy-bg-se {
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_se.png', sizingMethod='scale');
        }

        .fancybox-ie #fancy-bg-s {
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_s.png', sizingMethod='scale');
        }

        .fancybox-ie #fancy-bg-sw {
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_sw.png', sizingMethod='scale');
        }

        .fancybox-ie #fancy-bg-w {
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_w.png', sizingMethod='scale');
        }

        .fancybox-ie #fancy-bg-nw {
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_nw.png', sizingMethod='scale');
        }
    </style>
    <![endif]-->
    <!-- VK -->

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
    <script type="text/javascript">
        VK.init({apiId: 5015254, onlyWidgets: true});
    </script>

    <!-- VK -->

    <!-- FACEBOOK -->

    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.4&appId=211927395664390";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <!-- FACEBOOK -->

<? if (is_array($arResult['DETAIL_PICTURE_350']) || count($arResult["MORE_PHOTO"]) > 0): ?>
    <script type="text/javascript">
        $(function () {
            $('.product-img-fancy').fancybox({
                'arrows': true,
                'onComplete': function () {
                    $("#fancybox-title").css({'top': '100%', 'bottom': 'auto'});
                }
            });
        });
        //$('.imgContain').on("click","img", function (e) {
        $('.imgContain').live('click', function() {
            //$(".imgContain").click(function(){
            //e.preventDefault();
            //elem = $("a").find("[thumbimg='" + $(this).attr("thumbimgid") + "']");
            //$('#panel').prepend($('#panel elem'))
            //$( "a[thumbimg='350']" ).attr('class')
            $("a").find("[thumbimg='" + $(this).attr("thumbimgid") + "']").prependTo("#panel");
            $( "a[thumbimg='"+$(this).attr("thumbimgid")+"']" ).prependTo("#panel");
            console.log($(this).attr("thumbimgid"));
        });
    </script>
    <style type="text/css">
        a.product-img-fancy {
            display: none;
        }

        a.product-img-fancy:first-child {
            display: block;
        }
    </style>
<? endif; ?>

<?
$arResult['DETAIL_PICTURE_350']["WIDTH"] = 225;
$arResult['DETAIL_PICTURE_350']["HEIGHT"] = 190;
?>

    <div class="product-card">
        <div class="product-card--right-block">


                <div class="product-img" id="panel">
                    <? if (is_array($arResult['DETAIL_PICTURE_350'])): ?>
                        <a thumbimg="350" href="<?= $arResult['DETAIL_PICTURE_350']['SRC'] ?>" rel="gallery1" class="product-img-fancy"
                           id="largeLinks"><img itemprop="image" src="<?= $arResult['DETAIL_PICTURE_350']['SRC'] ?>"
                                                alt="<?= $arResult["NAME"] ?>" id="largeImage"
                                                width="<?= $arResult['DETAIL_PICTURE_350']["WIDTH"] ?>"/></a>
                    <? endif; ?>
                    <? if (count($arResult["MORE_PHOTO"]) > 0):
                        foreach ($arResult["MORE_PHOTO"] as $PHOTO):
                            ?>
                            <a thumbimg="<?=$PHOTO['ID']?>" href="<?= $PHOTO["SRC_PREVIEW"] ?>" rel="gallery1" class="product-img-fancy">
                                <img border="0" src="<?= $PHOTO["SRC_PREVIEW"] ?>" width="<?= $PHOTO["PREVIEW_WIDTH"] ?>"
                                     alt="<?= $arResult["NAME"] ?>"/>
                            </a>
                            <?
                        endforeach;
                    endif ?>
                </div>
                <div class="product-img-thumbs" id="">
                    <? if (is_array($arResult['DETAIL_PICTURE_350'])): ?>
                        <? //var_dump($arResult['DETAIL_PICTURE_350']);?>
                        <div class="product-img-thumbs__item">
                            <img thumbimgid="350" class="imgContain" src="<?= $arResult['DETAIL_PICTURE_350']['SRC'] ?>" alt="<?= $arResult["NAME"] ?>"
                                 width="<?= $arResult['DETAIL_PICTURE_350']["WIDTH"] ?>"/>
                        </div>
                    <? endif; ?>

                    <? if (count($arResult["MORE_PHOTO"]) > 0):
                        foreach ($arResult["MORE_PHOTO"] as $PHOTO):
                            //var_dump($PHOTO);
                            ?>
                            <div class="product-img-thumbs__item">
                                <img thumbimgid="<?=$PHOTO['ID']?>" class="imgContain" border="0" src="<?= $PHOTO["SRC_PREVIEW"] ?>" width="<?= $PHOTO["PREVIEW_WIDTH"] ?>"
                                     alt="<?= $arResult["NAME"] ?>"/>
                            </div>
                            <?
                        endforeach;
                    endif ?>
                </div>

            <div class="product-social">
                <!-- LIKEMODULE -->
                <div id="vk_like"></div>
                <script type="text/javascript">
                    VK.Widgets.Like("vk_like", {type: "mini"});
                </script>
                <div class="fb-like" data-width="200" data-layout="button_count" data-action="like"
                     data-show-faces="true" data-share="false"></div>

                <!-- LIKEMODULE -->
                <script>
                    $('#thumbs').delegate('img', 'click', function () {
                        $('#largeImage').attr('src', $(this).attr('src').replace('thumb', 'large'));
                        $('#largeLinks').attr('href', $(this).attr('src').replace('thumb', 'large'));
                        $('#description').html($(this).attr('alt'));
                    });
                </script>
            </div>
            <? if ((strlen($arResult["PROPERTIES"]["BRAND"]["VALUE"]) > 0) && (strlen($arResult["PROPERTIES"]["BRAND"]["PROPS"]["PREVIEW_TEXT"]) > 0)): ?>
                <div class="manufacture">
                    <div class="manufacture--title">�������������</div>
                    <? if (is_array($arResult["PROPERTIES"]["BRAND"]["PROPS"]["PREVIEW_PICTURE"])): ?>
                        <div class="manufacture--subtitle">
                            <img src="<?= $arResult["PROPERTIES"]["BRAND"]["PROPS"]["PREVIEW_PICTURE"]['SRC'] ?>"
                                 alt="<?= $arResult["PROPERTIES"]["BRAND"]["PROPS"]["NAME"] ?>"/>
                        </div>
                    <? endif; ?>
                    <div class="manafacture--desc">
                        <? echo strip_tags($arResult["PROPERTIES"]["BRAND"]["PROPS"]["PREVIEW_TEXT"], '<a>'); ?>
                        <a class="extra-link" href="#">���������</a>
                    </div>
                </div>
            <? endif; ?>

        </div>

        <? // moved to component epilog
        /*div id="form_1" class="fancyboxform" style="display:none;">
        <?$APPLICATION->IncludeComponent(
                "starbike:form.result.new",
                "fancyboxform_1",
                array(
                    "SEF_MODE" => "Y",
                    "WEB_FORM_ID" => "1",
                    "LIST_URL" => "",
                    "EDIT_URL" => "",
                    "SUCCESS_URL" => "",
                    "CHAIN_ITEM_TEXT" => "",
                    "CHAIN_ITEM_LINK" => "",
                    "IGNORE_CUSTOM_TEMPLATE" => "Y",
                    "USE_EXTENDED_ERRORS" => "Y",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "3600",
                    "SEF_FOLDER" => "/",
                    "COMPONENT_TEMPLATE" => "fancyboxform_1"
                ),
                $component
                );?>
        </div*/ ?>
        <div class="product-card--left-block">
            <div class="row align-bottom">
                <div class="card-prices-block columns large-8">
                    <div class="code-product">
                        <div class="code-product__item"><span
                                    class="title">�������:</span> <? if ($arResult["DISPLAY_PROPERTIES"]["ARTNUMBER"]["DISPLAY_VALUE"]) echo $arResult["DISPLAY_PROPERTIES"]["ARTNUMBER"]["DISPLAY_VALUE"]; else echo '__________'; ?>
                        </div>

                        <? if (strlen($arResult["PROPERTIES"]["CATNUMBER"]["VALUE"]) > 0): ?>
                            <div class="code-product__item"><span
                                        class="title">���:</span> <?= $arResult["PROPERTIES"]["CATNUMBER"]["VALUE"] ?>
                            </div>
                        <? endif; ?>

                        <? if (strlen($arResult["PROPERTIES"]["PRESENSE"]["VALUE"]) > 0): ?>
                            <div class="code-product__item"><?= $arResult["PROPERTIES"]["PRESENSE"]["VALUE"] ?></div>
                        <? endif; ?>
                    </div>

                    <div class="sales-and-prices">

                        <? foreach ($arResult["PRICES"] as $code => $arPrice):
                            if ($arPrice["CAN_ACCESS"]):
                                ?>
                                <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                                <div class="sale"><?= $arPrice["PRINT_VALUE"] ?></div>
                                <div class="price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></div>
                                <?
                            else:?>
                                <div class="price"><?= $arPrice["PRINT_VALUE"] ?></div>
                            <? endif; ?> <!-- DISCOUNT_VALUE -->
                                <?
                                break;
                            endif; // can access
                        endforeach;
                        ?>
                    </div>
                </div>

                <? if ($arResult["CAN_BUY"] && ($arResult["PROPERTIES"]["PRESENSE"]["VALUE_XML_ID"] != "outofstock")): ?>
                    <div class="columns large-4">
                        <div class="add-cart">
                            <a href="/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=<?= $arResult["CATALOG_PRICE_ID_1"] ?>&PRODUCT_ID=<?= $arResult["ID"] ?>"
                               class="add-cart__button" rel="nofollow"
                               onclick="return addToCart(this, 'catalog_detail_image', 'detail', '<?= GetMessage("CATALOG_IN_BASKET") ?>');"
                               id="catalog_add2cart_link">������</a>
                            <? /*a href="<?=$arResult["ADD_URL"]?>" class="add-cart__button" rel="nofollow" onclick="return addToCart(this, 'catalog_detail_image', 'detail', '<?=GetMessage("CATALOG_IN_BASKET")?>');" id="catalog_add2cart_link">������</a*/ ?>
                        </div>
                    </div>

                <? else: // ---------------------- Big eLse below is outofstock ?>


                    <div class="columns large-4">
                        <div class="add-cart">
                            <a rel="nofollow" class="fancybox add-cart__button" rel="group" href="#form_2">���������
                                ������</a>
                        </div>
                    </div>


                <? endif; ?> <!-- CLOSE the checking of outstock items -->
            </div> <!-- row-->

            <div class="row align-middle">
                <div class="columns large-6" style="padding-left:0;">

                    <div class="feedback">
                        <div class="feedback-item feedback-item--left"><a rel="nofollow"
                                                                          class="fancybox fancyboxform-button"
                                                                          rel="group" href="#form_3">������ ������</a>
                        </div>
                        <div class="feedback-item feedback-item--right"><a rel="nofollow"
                                                                           class="fancybox fancyboxform-button"
                                                                           rel="group" href="#form_1">����� �������?</a>
                        </div>
                    </div>

                </div>
            </div>


            <? if (!$arResult["CAN_BUY"] && (count($arResult["PRICES"]) > 0)): ?>
                <p class="buy_g">
                    <strong>
                        <? foreach ($arResult["PRICES"] as $code => $arPrice):
                            if ($arPrice["CAN_ACCESS"]):
                                ?>
                                <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                                <?= $arPrice["PRINT_DISCOUNT_VALUE"] ?>
                                <s><?= $arPrice["PRINT_VALUE"] ?></s>
                                <?
                            else:?>
                                <?= $arPrice["PRINT_VALUE"] ?>
                            <? endif; ?>
                                <?
                                break;
                            endif;
                        endforeach;
                        ?>
                    </strong>
                    <!--noindex--><a><?= GetMessage("CATALOG_NOT_AVAILABLE"); ?></a><!--/noindex-->
                </p>
            <? endif; ?>
            <div class="product-description">
                <div class="product-description__text">
                    <? if ($arResult["DETAIL_TEXT"]): ?>
                        <?= $arResult["DETAIL_TEXT"]; ?>
                    <? else: ?>
                        <?= $arResult["PREVIEW_TEXT"]; ?>
                    <? endif; ?>
                </div>
            </div>
        </div>

    </div> <!-- product-card -->

<? if ($USER->GetID() == "15"): ?>
    <? //echo "<pre style='font-size: 11px; line-height: 1.2;'>"; print_r ($arResult); echo "</pre>"?>
<? endif; ?>


<?
if (is_array($arResult['DISPLAY_PROPERTIES']) && count($arResult['DISPLAY_PROPERTIES']) > 0):
    ?>
    <? $arProperty = $arResult["DISPLAY_PROPERTIES"]["RECOMMEND"] ?>

    <? if (count($arProperty["DISPLAY_VALUE"]) > 0): ?>
    <div class="rightside">
        <?
        global $arRecPrFilter;
        $arRecPrFilter["ID"] = $arResult["DISPLAY_PROPERTIES"]["RECOMMEND"]["VALUE"];
        $APPLICATION->IncludeComponent("bitrix:store.catalog.top", "catalog", array(
            "IBLOCK_TYPE" => "",
            "IBLOCK_ID" => "",
            "ELEMENT_SORT_FIELD" => "sort",
            "ELEMENT_SORT_ORDER" => "desc",
            "ELEMENT_COUNT" => $arParams["ELEMENT_COUNT"],
            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
            "BASKET_URL" => $arParams["BASKET_URL"],
            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "DISPLAY_COMPARE" => "N",
            "PRICE_CODE" => $arParams["PRICE_CODE"],
            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
            "FILTER_NAME" => "arRecPrFilter",
            "DISPLAY_IMG_WIDTH" => $arParams["DISPLAY_IMG_WIDTH"],
            "DISPLAY_IMG_HEIGHT" => $arParams["DISPLAY_IMG_HEIGHT"],
            "SHARPEN" => $arParams["SHARPEN"],
            "ELEMENT_COUNT" => 30,
        ),
            $component
        );
        ?>
    </div>
    <? unset($arResult["DISPLAY_PROPERTIES"]["RECOMMEND"]) ?>
<? endif; ?>
<? endif; ?>

<? $APPLICATION->IncludeComponent(
    "bitrix:store.catalog.random",
    "special_element",
    Array(
        "DISPLAY_IMG_WIDTH" => "105",
        "DISPLAY_IMG_HEIGHT" => "70",
        "SHARPEN" => "50",
        "IBLOCK_TYPE_ID" => "catalog",
        "IBLOCK_ID" => array("3"),
        "PARENT_SECTION" => $arResult["IBLOCK_SECTION_ID"],
        "RAND_COUNT" => "4",
        "DETAIL_URL" => "",
        "CACHE_TYPE" => "Y",
        "CACHE_TIME" => "180",
        "CACHE_GROUPS" => "Y",
    ),
    false
); ?>