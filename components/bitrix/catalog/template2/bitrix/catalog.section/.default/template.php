<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="title title-to-top">
	<h1><?=$arResult["NAME"];?></h1>
</div>

<script type="text/javascript">
$(function() {
	$('.sc_img a').fancybox({
		'transitionIn': 'elastic',
		'transitionOut': 'elastic',
		'speedIn': 600,
		'speedOut': 200,
		'overlayShow': true,
		'overlayOpacity': 0.5,
		'cyclic' : true,
		'padding': 20,
		'titlePosition': 'over',
		'onComplete': function() {
			$("#fancybox-title").css({ 'top': '100%', 'bottom': 'auto' });
		} 
	});
	
	$('.thumbs img').bind('click',function(){
		var id_photo = $(this).attr('rel');
		var largeImageSrc = $('#largeLinks_'+id_photo).attr('href');
		var largeImageAlt = $('#largeLinks_'+id_photo).attr('alt');
		var largeImageTitle = $('#largeLinks_'+id_photo).attr('title');
		$('#largeImage_'+id_photo).attr('src',$(this).attr('src'));
		$('#largeImage_'+id_photo).attr('alt',$(this).attr('alt'));
		$('#largeImage_'+id_photo).attr('title',$(this).attr('title'));
		$('#largeLinks_'+id_photo).attr('href',$(this).attr('src'));
		$('#largeLinks_'+id_photo).attr('title',$(this).attr('title'));
		$('#largeLinks_'+id_photo).attr('alt',$(this).attr('alt'));
		$('#description_'+id_photo).html($(this).attr('alt'));
		$(this).attr('src', largeImageSrc);
		$(this).attr('alt', largeImageAlt);
		$(this).attr('title', largeImageTitle);
	});
});

</script>



<div class="top-catalog-nav">


	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"];?>
	<?endif;?>
	<div style="clear:both;"></div>

	<?if(strlen($arResult["DESCRIPTION"])>0):?>
		<div class="section-description">
			<?=$arResult["DESCRIPTION"];?>
		</div>
	<?endif;?>

</div>


		<?
		foreach ($arResult['ITEMS'] as $key => $arElement):
			$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CATALOG_ELEMENT_DELETE_CONFIRM')));

			$bHasPicture = is_array($arElement['PREVIEW_IMG']);

			$sticker = "";
			if (array_key_exists("PROPERTIES", $arElement) && is_array($arElement["PROPERTIES"]))
			{
				foreach (Array("SPECIALOFFER", "NEWPRODUCT", "SALELEADER") as $propertyCode)
					if (array_key_exists($propertyCode, $arElement["PROPERTIES"]) && intval($arElement["PROPERTIES"][$propertyCode]["PROPERTY_VALUE_ID"]) > 0)
						$sticker .= "&nbsp;<span class=\"sticker\">".$arElement["PROPERTIES"][$propertyCode]["NAME"]."</span>";
			}

		?>
		<div class="catalog-product-item row<? if(is_array($arElement["PROPERTIES"]["GROUPS"])) { echo ' groupped';}?>">

			<?if(!is_array($arElement["PROPERTIES"]["GROUPS"])): // ���� �� ������?>
				<?if($bHasPicture && $arElement['PREVIEW_IMG']['SRC']!=''):?>
					<div class="catalog-product-item__img medium-2">
						<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>" width="<?=$arElement["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_IMG"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" /></a>
					</div>
				<?else:?>
					<div class="catalog-product-item__img medium-2">
						<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img  src="<?=SITE_TEMPLATE_PATH?>/images/no_images.png" width="144" height="121" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" /></a>
					</div>
				<?endif;?>
				<div class="sizes-product medium-10 row">
  				<div class="catalog-product-item__text column small-12">
					  <div class="title"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></div>
            <div class="text"><?=$arElement['PREVIEW_TEXT']?></div>
          </div>
					<div class="catalog-product-item__size clearfix">
						<div class="size-name"></div>
						<div class="size-check">
							<span class="check-order <?=$arElement["PROPERTIES"]["PRESENSE"]["VALUE_XML_ID"]?>">
							<?if(strlen($arElement["PROPERTIES"]["PRESENSE"]["VALUE"])>0):?>
								<?=$arElement["PROPERTIES"]["PRESENSE"]["VALUE"]?>
							<?endif;?>
							</span>
						</div>
								<?
								$type_for_price = 200;
								foreach($arElement["PRICES"] as $code=>$arPrice):
									if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):
										if(IntVal($arPrice["VALUE"]) > 10000) $type_for_price = 210;
									endif;
								endforeach;
								?>
								<div class="prices ssss">
								<?foreach($arElement["PRICES"] as $code=>$arPrice):
									if($arPrice["CAN_ACCESS"]):
								?>
									<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
										<div class="sale"><?=$arPrice["PRINT_VALUE"]?></div> <!-- ������ � ��������� ������� -->
										<div class="price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></div>
									<?else:?>
										<div class="price"><?=$arPrice["PRINT_VALUE"]?></div>
									<?endif;?>
								<?
									endif;
								endforeach;
								?>
								</div>

							<?if($arElement["CAN_BUY"] && ($arElement["PROPERTIES"]["PRESENSE"]["VALUE_XML_ID"] != "outofstock")):?>
								<div class="extra cart-check">
									<?$PriceCatalogProductInList = CPrice::GetBasePrice($arElement["ID"]);?>
									<a href="/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=<?=$PriceCatalogProductInList["ID"]?>&PRODUCT_ID=<?=$arElement["ID"]?>" class="t_buy" rel="nofollow"  onclick="return addToCart(this, 'catalog_list_image_<?=$arElement['ID']?>', 'list', '<?=GetMessage("CATALOG_IN_CART")?>');" id="catalog_add2cart_link_<?=$arElement['ID']?>"><?=GetMessage("CATALOG_BUY")?></a>
								</div>
							<?else:?>
								<div class="extra">
									<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=GetMessage('CATALOG_MORE')?></a>
								</div>
							<?endif;?>
					</div>
					
				</div>
				<?else: // ������?>
				
					<?if(count($arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"]) == 1){?>
						<?if($bHasPicture && $arElement['PREVIEW_IMG']['SRC']!=''):?>
							<div class="catalog-product-item__img medium-2">
								<img src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>" width="<?=$arElement["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_IMG"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" />
							</div>
						<?else:?>
							<div class="catalog-product-item__img medium-2">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/no_images.png" width="136" height="121" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" />
							</div>
						<?endif;?>
					<?}else{?>
						<div class="catalog-product-item__img medium-2">
						<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" id="largeLinks_<?=$arElement["PROPERTIES"]["GROUPS"]["ID"][0]?>" alt="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>" title="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>"><img id="largeImage_<?=$arElement["PROPERTIES"]["GROUPS"]["ID"][0]?>" src="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['src']?>" width="136" alt="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>" title="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>" /></a>
						<!-- <div class="thumbs">
						<?
						if(count($arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"]) > 1){
							foreach($arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"] as $key_photo=>$val_photo){
								if($key_photo == 0) continue;
							?>
								<img src="<?=$val_photo['src']?>" class="small" alt="<?=$val_photo['description']?>" title="<?=$val_photo['description']?>" rel="<?=$arElement["PROPERTIES"]["GROUPS"]["ID"][0]?>" />
						<?
							}
						}
						?>
						</div> -->
						</div>
					<?}?>
						<?if((strlen($arResult["PROPERTIES"]["BRAND"]["VALUE"])>0) && (strlen($arResult["PROPERTIES"]["BRAND"]["PROPS"]["PREVIEW_TEXT"])>0)):?>
						
							<?if (is_array($arResult["PROPERTIES"]["BRAND"]["PROPS"]["PREVIEW_PICTURE"])):?>
							<div class="catalog-product-item__manufacture">
								<img src="<?=$arResult["PROPERTIES"]["BRAND"]["PROPS"]["PREVIEW_PICTURE"]['SRC']?>" alt="<?=$arResult["PROPERTIES"]["BRAND"]["PROPS"]["NAME"]?>" />
							</div>
							<?endif;?>
							<?echo strip_tags($arResult["PROPERTIES"]["BRAND"]["PROPS"]["PREVIEW_TEXT"], '<a>');?>
						
						<?endif;?>
				<?endif;?>
				
				

				
				<?if(is_array($arElement["PROPERTIES"]["GROUPS"])):?>
					<div class="sizes-product medium-10 row">
            <div class="catalog-product-item__text column small-12">
					  	<div class="title"><?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_NAME"]?></div>
              <div class="text"><?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_DESC"];?></div>
            </div>
							
							<?
//echo "<pre>";
//var_dump($arElement["PROPERTIES"]["PRESENSE"]);
//echo "</pre>";
							foreach($arElement["PROPERTIES"]["GROUPS"]["ID"] as $kRec=>$valRec):
								$resRecommend = CIBlockElement::GetByID($valRec)->GetNext();
								// ��������� ���� ����� ���������� ������ ~~~~~~~~~~~~
									$PriceCatalogProduct = CPrice::GetBasePrice($resRecommend["ID"]);
									$PriceProduct = $PriceDiscountProduct = $PriceCatalogProduct["PRICE"]; // ������� ���� ������
									
									$dbProductDiscounts = CCatalogDiscount::GetList(
												array("SORT" => "DESC"), 
												array("+PRODUCT_ID" => $resRecommend["ID"], "ACTIVE" => "Y", "!>ACTIVE_FROM" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")), "!<ACTIVE_TO" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")), "COUPON" => ""), false, false, 
												array("ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO", "RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE", "VALUE", "CURRENCY", "PRODUCT_ID")
										);
									while ($arProductDiscounts = $dbProductDiscounts->Fetch())
									{								
										// ���� �� �������
										if($arProductDiscounts["VALUE_TYPE"] == "P")
											$PriceDiscountProduct = ($PriceProduct - ($PriceProduct * ($arProductDiscounts["VALUE"]/100))); 
										else
											$PriceDiscountProduct = $PriceProduct - $arProductDiscounts["VALUE"]; 
									}
									
								//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
								if(strlen(htmlspecialchars_decode($arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec])) > 25) $arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec] = substr(htmlspecialchars_decode($arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec]),0,24).'...';
							?>
							<div class="catalog-product-item__size clearfix">
									<div class="size-name" id="catalog_recomend_list_<?=$resRecommend['ID']?>">
										<a class="size-name_link" href="<?=$arElement["PROPERTIES"]["GROUPS"]["DETAIL_PAGE_URL"][$kRec]?>"><?=$arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec];?></a>
									</div>
<?
$db_props = CIBlockElement::GetProperty($resRecommend['IBLOCK_ID'], $resRecommend['ID'], array("sort" => "asc"), Array("CODE"=>"PRESENSE"));
if($ar_props = $db_props->Fetch()){
	$FORUM_TOPIC_ID = ($ar_props["VALUE_ENUM"]);
	//echo "<pre>";
	//var_dump($ar_props);
	//echo "</pre>";
$FORUM_TOPIC_XML_ID = ($ar_props["VALUE_XML_ID"]);
}else{
    $FORUM_TOPIC_ID = false;
}
?>
									<div class="size-check">
										<span class="check-order <?=$FORUM_TOPIC_XML_ID?>">
										<?if(strlen($FORUM_TOPIC_ID)>0):?>
											<?=$FORUM_TOPIC_ID?>
										<?endif;?>
										</span>
									</div>

									<div class="prices">
										<?
										if($PriceProduct != $PriceDiscountProduct){// �������� ���� ������ � ������ ������
										?>
										<div class="sale"><?=number_format($PriceProduct, 0, '', ' ');?>&nbsp;���.</div>
										<div class="price"><?=number_format($PriceDiscountProduct, 0, '', ' ');?>&nbsp;���.</div>
										<?}else{?>
											<div class="price"><?=number_format($PriceProduct, 0, '', ' ');?>&nbsp;���.</div>
										<?}?>
									</div>

								<?if($arElement["PROPERTIES"]["GROUPS"]["PRESENSE"][$kRec]["VALUE_XML_ID"] != "outofstock"):?>
									<div class="extra cart-check">
										<a href="/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=<?=$PriceCatalogProduct["ID"]?>&PRODUCT_ID=<?=$resRecommend["ID"]?>" class="t_buy" onclick="return addToCart(this, 'catalog_recomend_list_<?=$resRecommend['ID']?>', 'list', '<?=GetMessage("CATALOG_IN_CART")?>');"><?=GetMessage("CATALOG_BUY")?></a>
									</div>
								<?else:?>
									<div class="extra">
										<a href="<?=$arElement["PROPERTIES"]["GROUPS"]["DETAIL_PAGE_URL"][$kRec]?>"><?=GetMessage('CATALOG_MORE')?></a>
									</div>
								<?endif;?>

							</div>
						<?endforeach;?>
						
					</div>
				<?endif;?>
				


		</div>
<?endforeach;?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"];?>
<?endif;?>


<?/*if ($USER->GetID() == "15"): ?>
<?echo "<pre>"; print_r ($arResult); echo "</pre>"?>
<?endif;*/?>