<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!--[if IE]>
<style type="text/css">
	#fancybox-loading.fancybox-ie div	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_loading.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-close		{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_close.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-title-over	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_over.png', sizingMethod='scale'); zoom: 1; }
	.fancybox-ie #fancybox-title-left	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_left.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-title-main	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_main.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-title-right	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_right.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-left-ico		{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_nav_left.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-right-ico	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_nav_right.png', sizingMethod='scale'); }
	.fancybox-ie .fancy-bg { background: transparent !important; }
	.fancybox-ie #fancy-bg-n	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_n.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-ne	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_ne.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-e	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_e.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-se	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_se.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-s	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_s.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-sw	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_sw.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-w	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_w.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-nw	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_nw.png', sizingMethod='scale'); }
</style>
<![endif]-->
<!-- VK -->

<script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
<script type="text/javascript">
  VK.init({apiId: 5015254, onlyWidgets: true});
</script>

<!-- VK -->

<!-- FACEBOOK -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.4&appId=211927395664390";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- FACEBOOK -->

<?if (is_array($arResult['DETAIL_PICTURE_350']) || count($arResult["MORE_PHOTO"])>0):?>
<script type="text/javascript">
$(function() {
	$('#panel a').fancybox({
		'transitionIn': 'elastic',
		'transitionOut': 'elastic',
		'speedIn': 600,
		'speedOut': 200,
		'overlayShow': true,
		'overlayOpacity': 0.5,
		'cyclic' : true,
		'padding': 20,
		'titlePosition': 'over',
		'onComplete': function() {
			$("#fancybox-title").css({ 'top': '100%', 'bottom': 'auto' });
		} 
	});
});
</script>
<?endif;?> 

<?
$arResult['DETAIL_PICTURE_350']["WIDTH"] = 225;
$arResult['DETAIL_PICTURE_350']["HEIGHT"] = 190;
?>

<div class="good">
	<div class="good_sl">
		<div id="ramka">
			<div id="gallery">
				<div id="panel">
					<?if (is_array($arResult['DETAIL_PICTURE_350'])):?>
						<a href="<?=$arResult['DETAIL_PICTURE_350']['SRC']?>" id="largeLinks"><img itemprop="image" src="<?=$arResult['DETAIL_PICTURE_350']['SRC']?>" alt="<?=$arResult["NAME"]?>" id="largeImage" width="<?=$arResult['DETAIL_PICTURE_350']["WIDTH"]?>" /></a>
					<?endif;?>
				</div>

				<div id="thumbs">
					<?if (is_array($arResult['DETAIL_PICTURE_350'])):?>
						<img src="<?=$arResult['DETAIL_PICTURE_350']['SRC']?>" alt="<?=$arResult["NAME"]?>" width="<?=$arResult['DETAIL_PICTURE_350']["WIDTH"]?>" />
					<?endif;?>
					
					<?if(count($arResult["MORE_PHOTO"])>0):
						foreach($arResult["MORE_PHOTO"] as $PHOTO):
					?>
						<img border="0" src="<?=$PHOTO["SRC_PREVIEW"]?>" width="<?=$PHOTO["PREVIEW_WIDTH"]?>" alt="<?=$arResult["NAME"]?>" />
					<?
						endforeach;
					endif?>
				</div>
			</div>
		</div>
<!-- LIKEMODULE -->
<div id="vk_like"></div>
<script type="text/javascript">
VK.Widgets.Like("vk_like", {type: "mini"});
</script>
<br/>

<div class="fb-like" data-width="200" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>

<!-- LIKEMODULE -->
<script>
		$('#thumbs').delegate('img','click', function(){
			$('#largeImage').attr('src',$(this).attr('src').replace('thumb','large'));
			$('#largeLinks').attr('href',$(this).attr('src').replace('thumb','large'));
			$('#description').html($(this).attr('alt'));
		});
		</script>
	</div>
	<div class="top_good">
		<h1><?=$arResult["NAME"];?></h1>
		<span>�������: <?if($arResult["DISPLAY_PROPERTIES"]["ARTNUMBER"]["DISPLAY_VALUE"]) echo $arResult["DISPLAY_PROPERTIES"]["ARTNUMBER"]["DISPLAY_VALUE"]; else echo '__________';?></span>
		<p class="buy_g">
			<strong>
			<?foreach($arResult["PRICES"] as $code=>$arPrice):
					if($arPrice["CAN_ACCESS"]):
				?>
					<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
						<?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
						<s><?=$arPrice["PRINT_VALUE"]?></s>
					<?else:?>
						<?=$arPrice["PRINT_VALUE"]?>
					<?endif;?>
				<?
						break;
					endif;
				endforeach;
				?>
			</strong>
		<?if($arResult["CAN_BUY"]):?>
			<!--noindex--><a href="<?=$arResult["ADD_URL"]?>" rel="nofollow" onclick="return addToCart(this, 'catalog_detail_image', 'detail', '<?=GetMessage("CATALOG_IN_BASKET")?>');" id="catalog_add2cart_link">������</a><!--/noindex-->
		<?endif;?>
		
		<?if(!$arResult["CAN_BUY"] && (count($arResult["PRICES"]) > 0)):?>
			<!--noindex--><a><?=GetMessage("CATALOG_NOT_AVAILABLE");?></a><!--/noindex-->
		<?endif;?>
		</p>
	</div>
	<p class="good_sl_p">
	<?if($arResult["DETAIL_TEXT"]):?>
		<?=$arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?=$arResult["PREVIEW_TEXT"];?>
	<?endif;?>
	</p>
</div>

<?
if (is_array($arResult['DISPLAY_PROPERTIES']) && count($arResult['DISPLAY_PROPERTIES']) > 0):
?>
	<?$arProperty = $arResult["DISPLAY_PROPERTIES"]["RECOMMEND"]?>
	
	<?if(count($arProperty["DISPLAY_VALUE"]) > 0):?>
	<div class="rightside">
			<?
			global $arRecPrFilter;
			$arRecPrFilter["ID"] = $arResult["DISPLAY_PROPERTIES"]["RECOMMEND"]["VALUE"];
			$APPLICATION->IncludeComponent("bitrix:store.catalog.top", "catalog", array(
				"IBLOCK_TYPE" => "",
				"IBLOCK_ID" => "",
				"ELEMENT_SORT_FIELD" => "sort",
				"ELEMENT_SORT_ORDER" => "desc",
				"ELEMENT_COUNT" => $arParams["ELEMENT_COUNT"],
				"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"DISPLAY_COMPARE" => "N",
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"FILTER_NAME" => "arRecPrFilter",
				"DISPLAY_IMG_WIDTH"	 =>	$arParams["DISPLAY_IMG_WIDTH"],
				"DISPLAY_IMG_HEIGHT" =>	$arParams["DISPLAY_IMG_HEIGHT"],
				"SHARPEN" => $arParams["SHARPEN"],
				"ELEMENT_COUNT" => 30,
				),
				$component
			);
			?>
	</div>
	<?unset($arResult["DISPLAY_PROPERTIES"]["RECOMMEND"])?>
	<?endif;?>
<?endif;?>

<?$APPLICATION->IncludeComponent(
	"bitrix:store.catalog.random",
	"special_element",
	Array(
		"DISPLAY_IMG_WIDTH" => "105",
		"DISPLAY_IMG_HEIGHT" => "70",
		"SHARPEN" => "50",
		"IBLOCK_TYPE_ID" => "catalog",
		"IBLOCK_ID" => array("3"),
		"PARENT_SECTION" => $arResult["IBLOCK_SECTION_ID"],
		"RAND_COUNT" => "4",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "180",
		"CACHE_GROUPS" => "Y",
	),
	false
);?>
