<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!--[if IE]>
<style type="text/css">
	#fancybox-loading.fancybox-ie div	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_loading.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-close		{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_close.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-title-over	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_over.png', sizingMethod='scale'); zoom: 1; }
	.fancybox-ie #fancybox-title-left	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_left.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-title-main	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_main.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-title-right	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_right.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-left-ico		{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_nav_left.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-right-ico	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_nav_right.png', sizingMethod='scale'); }
	.fancybox-ie .fancy-bg { background: transparent !important; }
	.fancybox-ie #fancy-bg-n	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_n.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-ne	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_ne.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-e	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_e.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-se	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_se.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-s	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_s.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-sw	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_sw.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-w	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_w.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-nw	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_nw.png', sizingMethod='scale'); }
</style>
<![endif]-->

<?
$APPLICATION->AddHeadScript('/bitrix/templates/'.SITE_TEMPLATE_ID.'/jquery/fancybox/jquery.fancybox-1.3.1.pack.js');
$APPLICATION->SetAdditionalCSS('/bitrix/templates/'.SITE_TEMPLATE_ID.'/jquery/fancybox/jquery.fancybox-1.3.1.css');
?>

<script type="text/javascript">
$(function() {
	$('.sc_img a').fancybox({
		'transitionIn': 'elastic',
		'transitionOut': 'elastic',
		'speedIn': 600,
		'speedOut': 200,
		'overlayShow': true,
		'overlayOpacity': 0.5,
		'cyclic' : true,
		'padding': 20,
		'titlePosition': 'over',
		'onComplete': function() {
			$("#fancybox-title").css({ 'top': '100%', 'bottom': 'auto' });
		} 
	});
	
	$('.thumbs img').bind('click',function(){
		var id_photo = $(this).attr('rel');
		var largeImageSrc = $('#largeLinks_'+id_photo).attr('href');
		var largeImageAlt = $('#largeLinks_'+id_photo).attr('alt');
		var largeImageTitle = $('#largeLinks_'+id_photo).attr('title');
		$('#largeImage_'+id_photo).attr('src',$(this).attr('src'));
		$('#largeImage_'+id_photo).attr('alt',$(this).attr('alt'));
		$('#largeImage_'+id_photo).attr('title',$(this).attr('title'));
		$('#largeLinks_'+id_photo).attr('href',$(this).attr('src'));
		$('#largeLinks_'+id_photo).attr('title',$(this).attr('title'));
		$('#largeLinks_'+id_photo).attr('alt',$(this).attr('alt'));
		$('#description_'+id_photo).html($(this).attr('alt'));
		$(this).attr('src', largeImageSrc);
		$(this).attr('alt', largeImageAlt);
		$(this).attr('title', largeImageTitle);
	});
});

</script>

<div class="section_catalog">

	<div class="top_section">
		<?
		$arAvailableSort = array(
			"name" => Array("name", "asc"), 
			"price" => Array('PROPERTY_PRICEHIDDEN', "asc"),
		);

		$sort = array_key_exists("sort", $_REQUEST) && array_key_exists(ToLower($_REQUEST["sort"]), $arAvailableSort) ? $arAvailableSort[ToLower($_REQUEST["sort"])][0] : "name";
		$sort_order = array_key_exists("order", $_REQUEST) && in_array(ToLower($_REQUEST["order"]), Array("asc", "desc")) ? ToLower($_REQUEST["order"]) : $arAvailableSort[$sort][1];
		?>

		<?foreach ($arAvailableSort as $key => $val):
			$className = $sort == $val[0] ? ' selected' : '';
			if ($className) 
				$className .= $sort_order == 'asc' ? ' asc' : ' desc';
			$newSort = $sort == $val[0] ? $sort_order == 'desc' ? 'asc' : 'desc' : $arAvailableSort[$key][1];
		?>

		<a href="<?=$APPLICATION->GetCurPageParam('sort='.$key.'&order='.$newSort, 	array('sort', 'order'))?>" class="<?=$key?><?=$className?>" rel="nofollow"><?=GetMessage('SECT_SORT_'.$key)?><img src="/i/section_pic.jpg" /></a>
		
		<?endforeach;?>
	</div>

	<div class="section_content">
<?
foreach ($arResult['ITEMS'] as $key => $arElement):
	$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CATALOG_ELEMENT_DELETE_CONFIRM')));

	$bHasPicture = is_array($arElement['PREVIEW_IMG']);

	$sticker = "";
	if (array_key_exists("PROPERTIES", $arElement) && is_array($arElement["PROPERTIES"]))
	{
		foreach (Array("SPECIALOFFER", "NEWPRODUCT", "SALELEADER") as $propertyCode)
			if (array_key_exists($propertyCode, $arElement["PROPERTIES"]) && intval($arElement["PROPERTIES"][$propertyCode]["PROPERTY_VALUE_ID"]) > 0)
				$sticker .= "&nbsp;<span class=\"sticker\">".$arElement["PROPERTIES"][$propertyCode]["NAME"]."</span>";
	}

?>
		<div class="section_content_item">
			<div class="white_border mi">
			<?if(!is_array($arElement["PROPERTIES"]["GROUPS"])): // ���� �� ������?>
				<?if($bHasPicture && $arElement['PREVIEW_IMG']['SRC']!=''):?>
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>" width="<?=$arElement["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_IMG"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" /></a>
				<?else:?>
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img  src="<?=SITE_TEMPLATE_PATH?>/images/no_images.png" width="144" height="121" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" /></a>
				<?endif;?>
				<div class="sc_text" style="width: 530px;">
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a>
				</div>
				<div class="table_cost" style="margin-right:0px; width: 570px;">
					<table style="float:right; width: auto;">
						<tr>
							<td>
								<?
								$type_for_price = 200;
								foreach($arElement["PRICES"] as $code=>$arPrice):
									if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):
										if(IntVal($arPrice["VALUE"]) > 10000) $type_for_price = 210;
									endif;
								endforeach;
								?>
								<p class="t_price" style="width:<?=$type_for_price;?>px;">
								<?foreach($arElement["PRICES"] as $code=>$arPrice):
									if($arPrice["CAN_ACCESS"]):
								?>
									<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
										<?=$arPrice["PRINT_DISCOUNT_VALUE"]?> / <span style="color: #3C3C3C; font-size: 13px; text-decoration: line-through;"><?=$arPrice["PRINT_VALUE"]?></span>
									<?else:?>
										<?=$arPrice["PRINT_VALUE"]?>
									<?endif;?>
								<?
									endif;
								endforeach;
								?>
								</p>
							</td>
							<td>
								<?if ($arElement['CAN_BUY']):?>
									<?$PriceCatalogProductInList = CPrice::GetBasePrice($arElement["ID"]);?>
									<a href="/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=<?=$PriceCatalogProductInList["ID"]?>&PRODUCT_ID=<?=$arElement["ID"]?>" class="t_buy" style="float: none; margin-top:0px;" rel="nofollow"  onclick="return addToCart(this, 'catalog_list_image_<?=$arElement['ID']?>', 'list', '<?=GetMessage("CATALOG_IN_CART")?>');" id="catalog_add2cart_link_<?=$arElement['ID']?>">������</a>
								<?elseif (count($arResult["PRICES"]) > 0):?>
									<span class="t_buy"><?=GetMessage('CATALOG_NOT_AVAILABLE')?></span>
								<?endif;?>	
							</td>
						</tr>
					</table>
					<p style="color: #000000; font-size: 12px; margin-top: 5px;"><?=$arElement['PREVIEW_TEXT']?></p>
				</div>
				<?else: // ������?>
				
					<?if(count($arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"]) == 1){?>
						<?if($bHasPicture && $arElement['PREVIEW_IMG']['SRC']!=''):?>
							<img src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>" width="<?=$arElement["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_IMG"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" />
						<?else:?>
							<img src="<?=SITE_TEMPLATE_PATH?>/images/no_images.png" width="136" height="121" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" />
						<?endif;?>
					<?}else{?>
						<div class="sc_img">
						<a href="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['src']?>" id="largeLinks_<?=$arElement["PROPERTIES"]["GROUPS"]["ID"][0]?>" alt="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>" title="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>"><img id="largeImage_<?=$arElement["PROPERTIES"]["GROUPS"]["ID"][0]?>" src="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['src']?>" width="136" alt="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>" title="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>" /></a>
						
						<div class="thumbs">
						<?
						if(count($arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"]) > 1){
							foreach($arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"] as $key_photo=>$val_photo){
								if($key_photo == 0) continue;
							?>
								<img src="<?=$val_photo['src']?>" class="small" alt="<?=$val_photo['description']?>" title="<?=$val_photo['description']?>" rel="<?=$arElement["PROPERTIES"]["GROUPS"]["ID"][0]?>" />
						<?
							}
						}
						?>
						</div>
						</div>
					<?}?>
					
					<div class="sc_text" style="width: 550px;">
						<a><?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_NAME"]?></a>
						<p style="color: #000000; font-size: 12px; margin-top: 5px;"><?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_DESC"];?></p>
					</div>
				
				<?endif;?>
				
				
				
				
				<?if(is_array($arElement["PROPERTIES"]["GROUPS"])):?>
					<div class="table_cost">
					<table>
					<?
					foreach($arElement["PROPERTIES"]["GROUPS"]["ID"] as $kRec=>$valRec):
						$resRecommend = CIBlockElement::GetByID($valRec)->GetNext();
						
						// ��������� ���� ����� ���������� ������ ~~~~~~~~~~~~
							$PriceCatalogProduct = CPrice::GetBasePrice($resRecommend["ID"]);
							$PriceProduct = $PriceDiscountProduct = $PriceCatalogProduct["PRICE"]; // ������� ���� ������
							
							$dbProductDiscounts = CCatalogDiscount::GetList(
										array("SORT" => "DESC"), 
										array("+PRODUCT_ID" => $resRecommend["ID"], "ACTIVE" => "Y", "!>ACTIVE_FROM" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")), "!<ACTIVE_TO" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")), "COUPON" => ""), false, false, 
										array("ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO", "RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE", "VALUE", "CURRENCY", "PRODUCT_ID")
								);
							while ($arProductDiscounts = $dbProductDiscounts->Fetch())
							{								
								// ���� �� �������
								if($arProductDiscounts["VALUE_TYPE"] == "P")
									$PriceDiscountProduct = ($PriceProduct - ($PriceProduct * ($arProductDiscounts["VALUE"]/100))); 
								else
									$PriceDiscountProduct = $PriceProduct - $arProductDiscounts["VALUE"]; 
							}
							
						//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						if(strlen(htmlspecialchars_decode($arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec])) > 25) $arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec] = substr(htmlspecialchars_decode($arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec]),0,24).'...';
					?>
						<tr>
							<td>
								<p class="t_name" id="catalog_recomend_list_<?=$resRecommend['ID']?>">
									<a style="color:#5C361F;" href="<?=$arElement["PROPERTIES"]["GROUPS"]["DETAIL_PAGE_URL"][$kRec]?>"><?=$arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec];?></a>
								</p>
							</td>
							<td>
								<p class="t_price" style="width:180px;">
									<?
									if($PriceProduct != $PriceDiscountProduct){// �������� ���� ������ � ������ ������
									?>
									<?=number_format($PriceDiscountProduct, 0, '', ' ');?>&nbsp;���./<span style="color: #3C3C3C; font-size: 13px; text-decoration: line-through;"><?=number_format($PriceProduct, 0, '', ' ');?>&nbsp;���.</span>
									<?}else{?>
										<?=number_format($PriceProduct, 0, '', ' ');?>&nbsp;���.
									<?}?>
								</p>
							</td>
							<td><a href="/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=<?=$PriceCatalogProduct["ID"]?>&PRODUCT_ID=<?=$resRecommend["ID"]?>" class="t_buy" onclick="return addToCart(this, 'catalog_recomend_list_<?=$resRecommend['ID']?>', 'list', '������');">������</a></td>
						</tr>
					<?endforeach;?>
					</table>
					</div>
				<?endif;?>
				
			</div>
		</div>
<?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"];?>
<?endif;?>
	
</div>