<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// cache hack to use items list in component_epilog.php
$this->__component->arResult["IDS"] = array();
$this->__component->arResult["OFFERS_IDS"] = array();

if(isset($arParams["DETAIL_URL"]) && strlen($arParams["DETAIL_URL"]) > 0)
	$urlTemplate = $arParams["DETAIL_URL"];
else
	$urlTemplate = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "DETAIL_PAGE_URL");

//2 Sections subtree
$arSections = array();
$rsSections = CIBlockSection::GetList(
	array(), 
	array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"LEFT_MARGIN" => $arResult["LEFT_MARGIN"],
		"RIGHT_MARGIN" => $arResult["RIGHT_MARGIN"],
	), 
	false, 
	array("ID", "DEPTH_LEVEL", "SECTION_PAGE_URL")
);

while($arSection = $rsSections->Fetch())
	$arSections[$arSection["ID"]] = $arSection;

	
$ActiveIDArray = $NoneIDArray = array();

foreach ($arResult["ITEMS"] as $key => $arElement) 
{
	$this->__component->arResult["IDS"][] = $arElement["ID"];
	
	if(is_array($arElement["OFFERS"]) && !empty($arElement["OFFERS"])){
		foreach($arElement["OFFERS"] as $arOffer){
			$this->__component->arResult["OFFERS_IDS"][] = $arOffer["ID"];
		}
	}
	
	if(is_array($arElement["PREVIEW_PICTURE"]))
	{
		$arFilter = '';
		if($arParams["SHARPEN"] != 0)
		{
			$arFilter = array(array("name" => "sharpen", "precision" => $arParams["SHARPEN"]));
		}
		$arFileTmp = CFile::ResizeImageGet(
			$arElement["PREVIEW_PICTURE"],
			array("width" => $arParams["DISPLAY_IMG_WIDTH"], "height" => $arParams["DISPLAY_IMG_HEIGHT"]),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true, $arFilter
		);

		$arResult["ITEMS"][$key]["PREVIEW_IMG"] = array(
			"SRC" => $arFileTmp["src"],
			'WIDTH' => $arFileTmp["width"],
			'HEIGHT' => $arFileTmp["height"],
		);
	}
	else{
		$arFilter = '';
		if($arParams["SHARPEN"] != 0)
		{
			$arFilter = array(array("name" => "sharpen", "precision" => $arParams["SHARPEN"]));
		}
		$arFileTmp = CFile::ResizeImageGet(
			$arElement["DETAIL_PICTURE"],
			array("width" => $arParams["DISPLAY_IMG_WIDTH"], "height" => $arParams["DISPLAY_IMG_HEIGHT"]),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true, $arFilter
		);

		$arResult["ITEMS"][$key]["PREVIEW_IMG"] = array(
			"SRC" => $arFileTmp["src"],
			'WIDTH' => $arFileTmp["width"],
			'HEIGHT' => $arFileTmp["height"],
		);
	}
	
	$section_id = $arElement["~IBLOCK_SECTION_ID"];

	if(array_key_exists($section_id, $arSections))
	{
		$urlSection = str_replace(
			array("#SECTION_ID#", "#SECTION_CODE#"),
			array($arSections[$section_id]["ID"], $arSections[$section_id]["CODE"]),
			$urlTemplate
		);

		$arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = CIBlock::ReplaceDetailUrl(
			$urlSection,
			$arElement,
			true,
			"E"
		);
	}	
	
	// ����������� --------------------------------------------------
	$arGroupsItems = array();
	$arElement["PROPERTIES"]["GROUPS"] = $arGroupsItems;
	
	if(strlen($arElement["PROPERTIES"]["GROUPSNAME"]["VALUE"]) > 0){
		$nameGroup = $descGroup = $descGroup_preview = '';
		$picGroup = $picArrGroup = array();
		$ActiveID = $cell_descr = 0;
		
		foreach($arResult["ITEMS"] as $kgr=>$vgr):
			if($arElement["PROPERTIES"]["GROUPSNAME"]["VALUE"] == $vgr["PROPERTIES"]["GROUPSNAME"]["VALUE"]){
				$arGroupsItems["ID"][] = $vgr["ID"];
				$arGroupsItems["DETAIL_PAGE_URL"][] = $vgr["DETAIL_PAGE_URL"];
				$arGroupsItems["PRENAME"][] = $vgr["PROPERTIES"]["PRENAME"]["VALUE"] ? $vgr["PROPERTIES"]["PRENAME"]["VALUE"] : $vgr["NAME"];
				if($nameGroup == '') $nameGroup = $vgr["PROPERTIES"]["GROUPSNAME"]["VALUE"];
				if(strlen($descGroup) < strlen($vgr["PROPERTIES"]["GROUPSDESC"]["VALUE"]["TEXT"])){
					$descGroup = htmlspecialchars_decode($vgr["PROPERTIES"]["GROUPSDESC"]["VALUE"]["TEXT"]);
				}
				if(count($picGroup) == 0){
					$picGroup = $vgr["PREVIEW_IMG"];
					if(is_array($vgr["PROPERTIES"]["GROUPSPHOTO"]["VALUE"])):
						foreach($vgr["PROPERTIES"]["GROUPSPHOTO"]["VALUE"] as $key_photo=>$val_photo):
							$currPhoto = CFile::GetByID($val_photo)->Fetch();
							$currPhoto_tmp = explode('.', $currPhoto["ORIGINAL_NAME"]);
							$currPhoto["ORIGINAL_NAME"] = $currPhoto_tmp[0];
							$picArrGroup[] = array('src'=>CFile::GetPath($val_photo), 'description'=>$currPhoto["ORIGINAL_NAME"]);
						endforeach;
					else:
						$picArrGroup[] = $vgr["PREVIEW_IMG"]["SRC"];
					endif;
					
				}
				
				if($cell_descr == 0){
					$descGroup_preview = $vgr["PREVIEW_TEXT"];
				}
				
				if($ActiveID == 0)
					$ActiveIDArray[] = $vgr["ID"];
				else
					$NoneIDArray[] = $vgr["ID"];
				$ActiveID++;
				$cell_descr++;
			}
		endforeach;

		if(strlen($descGroup) < 15) $descGroup = $descGroup_preview;
		$arGroupsItems["GROUPS_NAME"] = $nameGroup; // name groups
		$arGroupsItems["GROUPS_DESC"] = $descGroup; // describtion groups
		$arGroupsItems["GROUPS_PIC"] = $picGroup; // picture groups
		$arGroupsItems["GROUPS_PHOTO"] = $picArrGroup; // picture groups
		
		#echo '<pre>'.print_r($arGroupsItems["GROUPS_PHOTO"],true).'</pre>';
		
		$arResult["ITEMS"][$key]["PROPERTIES"]["GROUPS"] = $arGroupsItems;
	}
	#trace($arElement["PROPERTIES"]["GROUPS"]);
	// --------------------------------------------------------------
	
}

$ActiveIDArray = array_unique($ActiveIDArray);
$NoneIDArray = array_unique($NoneIDArray);
#trace($ActiveIDArray);
#trace($NoneIDArray);

// �������� .. ������� ������ ������� ��� ������ � ������
foreach ($arResult["ITEMS"] as $key => $arElement):
	if(in_array($arElement["ID"], $NoneIDArray))
		unset($arResult["ITEMS"][$key]);
endforeach;


//���������� �� ������� ����������, ����� �� �����

foreach ($arResult["ITEMS"] as $key => $row) {
    $name[$key]  = $row['NAME'];
    $sort[$key] = $row['SORT'];
}

$name = array_map('strtolower', $name);

array_multisort($sort, SORT_ASC, $name, SORT_ASC, SORT_STRING, $arResult["ITEMS"]);



$this->__component->SetResultCacheKeys(array("IDS"));
$this->__component->SetResultCacheKeys(array("OFFERS_IDS"));
?>