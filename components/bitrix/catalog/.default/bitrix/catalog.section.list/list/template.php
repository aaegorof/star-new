<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (count($arResult["SECTIONS"]) == 0):?>
	<?=GetMessage('CATALOG_EMPTY_CATALOG');?>
<?endif;?>

<?
#trace($arResult["SECTIONS"]);
$ACTIVE_SECTION_ID = $ACTIVE_CURRENT_DEPTH = 0;
$NUM_COLS = 4;
$CURRENT_DEPTH=$arResult["SECTION"]["DEPTH_LEVEL"]+1;
foreach($arResult["SECTIONS"] as $arSection):

	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CATALOG_SECTION_DELETE_CONFIRM')));	

	$bHasPicture = is_array($arSection['PICTURE_PREVIEW']);
	$bHasChildren = is_array($arSection['CHILDREN']) && count($arSection['CHILDREN']) > 0;
	$STATUS = 0;
	
	$UP_SECTION_ID = $arSection['ID'];
	
	if($arParams["ACTIVE_SECTION_CODE"] == $arSection['CODE']){
		$STATUS = 1;
		$UP_SECTION_ID = $arSection['ID'];
		$ACTIVE_SECTION_ID = $arSection['ID'];
		$ACTIVE_CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
	}
	
	if($bHasChildren):	
		$cell = 0;
			foreach ($arSection['CHILDREN'] as $key => $arChild):
				if($arParams["ACTIVE_SECTION_CODE"] == $arChild['CODE']){
					$STATUS = 1;
					$UP_SECTION_ID = $arChild['ID'];
					$ACTIVE_SECTION_ID = $arChild['ID'];
					$ACTIVE_CURRENT_DEPTH = $arChild["DEPTH_LEVEL"];
					break;
				}
			endforeach;
	endif;
	
	if($STATUS) break;
	
endforeach;?>

<div class="navigation" style="margin-left:10px;" id="navigation_catalog">
	<div class="levels">
		<a href="/index.php#catalog">�������</a><?if($STATUS != 0){?><p>-></p><?}?>
		<?
		$nav = CIBlockSection::GetNavChain(false, $ACTIVE_SECTION_ID);
		while($ar_nav = $nav->GetNext()):
			?>
			<a href="<?=$ar_nav["SECTION_PAGE_URL"];?>"><?=$ar_nav["NAME"];?></a>
			<?if($ACTIVE_CURRENT_DEPTH != $ar_nav["DEPTH_LEVEL"]){?><p>-></p><?}?>
			<?
		endwhile;
		?>
	</div>
	<div class="levels_bottom">
	<?
	$cell = 0;
	foreach($arResult["SECTIONS"] as $arSection):
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT"));
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CATALOG_SECTION_DELETE_CONFIRM')));	

		$bHasPicture = is_array($arSection['PICTURE_PREVIEW']);
		$bHasChildren = is_array($arSection['CHILDREN']) && count($arSection['CHILDREN']) > 0;
		
		if($STATUS == 0):
			if($arSection["DEPTH_LEVEL"] == 1):
		?>
			<a href="<?=$arSection['SECTION_PAGE_URL'];?>"><?=$arSection['NAME'];?></a>
		<?
			endif;
		else:
			if($bHasChildren):	
					foreach ($arSection['CHILDREN'] as $key => $arChild):
						if($UP_SECTION_ID == $arChild['IBLOCK_SECTION_ID']){
						?>
							<a href="<?=$arChild['SECTION_PAGE_URL'];?>"><?=$arChild['NAME'];?></a>
						<?
							$cell++;
						}
					endforeach;
			endif;
		endif;
	endforeach;?>
	<?if($cell > 7) echo '<script language="javascript">$("#navigation_catalog").css("padding-bottom","15px");</script>'?>
	</div>
</div>