<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(count($arResult["LINKED_ELEMENTS"]) > 0){
	#trace($arResult["LINKED_ELEMENTS"]);
		foreach($arResult["LINKED_ELEMENTS"] as $key=>$arElement):
			$detail_page = $arElement["DETAIL_PAGE_URL"];
			$arElement = CIBlockElement::GetByID($arElement["ID"])->GetNext();
			$Props = BitrixSun::GetElementByIDWithProps($arElement["ID"]);
			
			$arFileTmp = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], array("width" => 144, "height" => 121), BX_RESIZE_IMAGE_PROPORTIONAL, true, '');
			$arElement["PREVIEW_IMG"] = array(
				"SRC" => $arFileTmp["src"],
				'WIDTH' => $arFileTmp["width"],
				'HEIGHT' => $arFileTmp["height"],
			);

			unset($arResult["LINKED_ELEMENTS"][$key]["DETAIL_PAGE_URL"]);
			$arResult["LINKED_ELEMENTS"][$key]["DETAIL_PAGE_URL"] = $detail_page;
			$arResult["ITEMS_LINKED"][] = array_merge($arElement, $Props);
		endforeach;

		foreach($arResult["ITEMS_LINKED"] as $key=>$arElement):
		// ����������� --------------------------------------------------
			$arGroupsItems = array();
			$arElement["PROPERTIES"]["GROUPS"] = $arGroupsItems;
			if(strlen($arElement["PROPERTIES"]["GROUPSNAME"]["VALUE"]) > 0){
				$nameGroup = $descGroup = '';
				$picGroup = $picArrGroup = array();
				$ActiveID = $cell_descr = 0;
				
				foreach($arResult["ITEMS_LINKED"] as $kgr=>$vgr):
					if($arElement["PROPERTIES"]["GROUPSNAME"]["VALUE"] == $vgr["PROPERTIES"]["GROUPSNAME"]["VALUE"]){
						$arGroupsItems["ID"][] = $vgr["ID"];
						
						$resSectionVGK = CIBlockSection::GetByID($vgr["IBLOCK_SECTION_ID"])->GetNext();
						$vgr["DETAIL_PAGE_URL"] = str_replace(array("#SECTION_CODE#", "#SITE_DIR#","#ELEMENT_CODE#"), array($resSectionVGK["CODE"], "", $vgr["CODE"]), $vgr["DETAIL_PAGE_URL"]);

				
						$arGroupsItems["DETAIL_PAGE_URL"][] = $vgr["DETAIL_PAGE_URL"];
                		$arGroupsItems["PRESENSE"][] = $vgr["PROPERTIES"]["PRESENSE"]; // new property 2016
						$arGroupsItems["PRENAME"][] = $vgr["PROPERTIES"]["PRENAME"]["VALUE"] ? $vgr["PROPERTIES"]["PRENAME"]["VALUE"] : $vgr["NAME"];
						if($nameGroup == '') $nameGroup = $vgr["PROPERTIES"]["GROUPSNAME"]["VALUE"];
						if(!empty($vgr["PROPERTIES"]["GROUPSDESC"]["VALUE"])){
							$descGroup = htmlspecialchars_decode($vgr["PROPERTIES"]["GROUPSDESC"]["VALUE"]["TEXT"]);
						}

						if(count($vgr["PROPERTIES"]["GROUPSPHOTO"]) != 0){
							$picGroup = $vgr["PREVIEW_IMG"];
							if(is_array($vgr["PROPERTIES"]["GROUPSPHOTO"])):
								foreach($vgr["PROPERTIES"]["GROUPSPHOTO"] as $key_photo=>$val_photo):
									$currPhoto_tmp = explode('.', $val_photo["FILE_PARAM"]["ORIGINAL_NAME"]);
									$currPhoto["ORIGINAL_NAME"] = $currPhoto_tmp[0];
									$picArrGroup[] = array('src'=>$val_photo["PATH"], 'description'=>$currPhoto["ORIGINAL_NAME"]);
								endforeach;
							else:
								$picArrGroup[] = $vgr["PREVIEW_IMG"]["SRC"];
							endif;
							
						}
						
						if($cell_descr == 0){
							$descGroup_preview = $vgr["PREVIEW_TEXT"];
						}
						
						if($ActiveID == 0)
							$ActiveIDArray[] = $vgr["ID"];
						else
							$NoneIDArray[] = $vgr["ID"];
						$ActiveID++;
						$cell_descr++;
					}
				endforeach;
				
				if(strlen($descGroup) < 15) $descGroup = $descGroup_preview;
				$arGroupsItems["GROUPS_NAME"] = $nameGroup; // name groups
				$arGroupsItems["GROUPS_DESC"] = $descGroup; // describtion groups
				$arGroupsItems["GROUPS_PIC"] = $picGroup; // picture groups
				$arGroupsItems["GROUPS_PHOTO"] = $picArrGroup; // picture groups

				$arResult["ITEMS_LINKED"][$key]["PROPERTIES"]["GROUPS"] = $arGroupsItems;
			}
			//trace($arElement["PROPERTIES"]["GROUPS"]);
			// --------------------------------------------------------------
		endforeach;

		$ActiveIDArray = array_unique($ActiveIDArray);
		$NoneIDArray = array_unique($NoneIDArray);

		// �������� .. ������� ������ ������� ��� ������ � ������
		foreach ($arResult["ITEMS_LINKED"] as $key => $arElement):
			if(in_array($arElement["ID"], $NoneIDArray))
				unset($arResult["ITEMS_LINKED"][$key]);
		endforeach;
}

//���������� �� ������� ����������, ����� �� ����
foreach ($arResult["ITEMS_LINKED"] as $key => $row) {
    $price[$key]  = $row["PROPERTIES"]["PRICEHIDDEN"]["VALUE"];
    $sort[$key] = $row['SORT'];
}
array_multisort($sort, SORT_DESC, $price, SORT_DESC, SORT_NUMERIC, $arResult["ITEMS_LINKED"]);

?>