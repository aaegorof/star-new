<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!--[if IE]>
<style type="text/css">
	#fancybox-loading.fancybox-ie div	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_loading.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-close		{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_close.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-title-over	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_over.png', sizingMethod='scale'); zoom: 1; }
	.fancybox-ie #fancybox-title-left	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_left.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-title-main	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_main.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-title-right	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_title_right.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-left-ico		{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_nav_left.png', sizingMethod='scale'); }
	.fancybox-ie #fancybox-right-ico	{ background: transparent; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_nav_right.png', sizingMethod='scale'); }
	.fancybox-ie .fancy-bg { background: transparent !important; }
	.fancybox-ie #fancy-bg-n	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_n.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-ne	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_ne.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-e	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_e.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-se	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_se.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-s	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_s.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-sw	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_sw.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-w	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_w.png', sizingMethod='scale'); }
	.fancybox-ie #fancy-bg-nw	{ filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/fancy_shadow_nw.png', sizingMethod='scale'); }
</style>
<![endif]-->

<?
$APPLICATION->AddHeadScript('/bitrix/templates/'.SITE_TEMPLATE_ID.'/jquery/fancybox/jquery.fancybox-1.3.1.pack.js');
$APPLICATION->SetAdditionalCSS('/bitrix/templates/'.SITE_TEMPLATE_ID.'/jquery/fancybox/jquery.fancybox-1.3.1.css');
?>
<script type="text/javascript">
$(function() {
	$('.sc_img a').fancybox({
		'transitionIn': 'elastic',
		'transitionOut': 'elastic',
		'speedIn': 600,
		'speedOut': 200,
		'overlayShow': true,
		'overlayOpacity': 0.5,
		'cyclic' : true,
		'padding': 20,
		'titlePosition': 'over',
		'onComplete': function() {
			$("#fancybox-title").css({ 'top': '100%', 'bottom': 'auto' });
		} 
	});
	
	$('.thumbs img').bind('click',function(){
		var id_photo = $(this).attr('rel');
		var largeImageSrc = $('#largeLinks_'+id_photo).attr('href');
		var largeImageAlt = $('#largeLinks_'+id_photo).attr('alt');
		var largeImageTitle = $('#largeLinks_'+id_photo).attr('title');
		$('#largeImage_'+id_photo).attr('src',$(this).attr('src'));
		$('#largeImage_'+id_photo).attr('alt',$(this).attr('alt'));
		$('#largeImage_'+id_photo).attr('title',$(this).attr('title'));
		$('#largeLinks_'+id_photo).attr('href',$(this).attr('src'));
		$('#largeLinks_'+id_photo).attr('title',$(this).attr('title'));
		$('#largeLinks_'+id_photo).attr('alt',$(this).attr('alt'));
		$('#description_'+id_photo).html($(this).attr('alt'));
		$(this).attr('src', largeImageSrc);
		$(this).attr('alt', largeImageAlt);
		$(this).attr('title', largeImageTitle);
	});
});

</script>
<h1 class="">�����: <?=$arResult["NAME"];?></h1>


<div class="brand-description row">

		<div class="column medium-3 large-2">
					<?if(is_array($arResult["PREVIEW_PICTURE"]) || is_array($arResult["DETAIL_PICTURE"])):?>
						<?if(is_array($arResult["PREVIEW_PICTURE"]) && is_array($arResult["DETAIL_PICTURE"])):?>
							<img border="0" style="border:3px #7D7D7D solid;" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arResult["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arResult["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" id="image_<?=$arResult["PREVIEW_PICTURE"]["ID"]?>" style="display:block;cursor:pointer;cursor: hand;" OnClick="document.getElementById('image_<?=$arResult["PREVIEW_PICTURE"]["ID"]?>').style.display='none';document.getElementById('image_<?=$arResult["DETAIL_PICTURE"]["ID"]?>').style.display='block'" />
							<img border="0" style="border:3px #7D7D7D solid;" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" id="image_<?=$arResult["DETAIL_PICTURE"]["ID"]?>" style="display:none;cursor:pointer; cursor: hand;" OnClick="document.getElementById('image_<?=$arResult["DETAIL_PICTURE"]["ID"]?>').style.display='none';document.getElementById('image_<?=$arResult["PREVIEW_PICTURE"]["ID"]?>').style.display='block'" />
						<?elseif(is_array($arResult["DETAIL_PICTURE"])):?>
							<img border="0" style="border:3px #7D7D7D solid;" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" />
						<?elseif(is_array($arResult["PREVIEW_PICTURE"])):?>
							<img border="0" style="border:3px #7D7D7D solid;" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arResult["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arResult["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" />
						<?endif?>
						<?if(count($arResult["MORE_PHOTO"])>0):?>
							<br /><a href="#more_photo"><?=GetMessage("CATALOG_MORE_PHOTO")?></a>
						<?endif;?>
				<?endif;?>

				<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
					<?=$arProperty["NAME"]?>:<b>&nbsp;<?
					if(is_array($arProperty["DISPLAY_VALUE"])):
						echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
					elseif($pid=="MANUAL"):
						?><a href="<?=$arProperty["VALUE"]?>"><?=GetMessage("CATALOG_DOWNLOAD")?></a><?
					else:
						echo $arProperty["DISPLAY_VALUE"];?>
					<?endif?></b>
				<?endforeach?>
				</div>
    <div class="column medium-9 large-10">
      <?if($arResult["DETAIL_TEXT"]):?>
      	<?=$arResult["DETAIL_TEXT"]?>
      <?elseif($arResult["PREVIEW_TEXT"]):?>
      	<?=$arResult["PREVIEW_TEXT"]?>
      <?endif;?>
	  </div>
	
</div>	


<div class="navigation" style="margin-left:10px;" id="navigation_catalog">

	<div class="levels_bottom">
	
	<?
		$cell = 0;
		$arSection = $SectName = array();
		$arFilterSection = Array("IBLOCK_ID"=>3, "PROPERTY_BRAND"=>$arResult["ID"], "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS"=>"Y");
		$resSection = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilterSection, Array("ID","IBLOCK_SECTION_ID"));
		while($arfieldsSection = $resSection->GetNext()){
			$db_old_groups = CIBlockElement::GetElementGroups($arfieldsSection["ID"], true);
			while($ar_group = $db_old_groups->Fetch())
				$arSection[] = $ar_group["ID"];
		}
		
		$arSection = array_unique($arSection);
		
		foreach($arSection as $valSection):
			$Objlist = CIBlockSection::GetByID($valSection)->GetNext();
			$SectName[$Objlist['NAME']] = array("NAME"=>$Objlist['NAME'], 'CODE'=>$Objlist['CODE'], "URL"=>$Objlist["SECTION_PAGE_URL"]);
		endforeach;
		ksort($SectName);
		
		if(count($arSection)>0){
			$count_ul = ceil((count($SectName))/3);
			foreach($SectName as $k=>$v){
				echo '<a href="'.$v["URL"].'?arrFilter_9='.abs(crc32($arResult["ID"])).'&set_filter=%CF%EE%EA%E0%E7%E0%F2%FC">'.$v["NAME"].'</a>'; //echo '<a href="'.$v["URL"].'?arrFilter_pf%5BBRAND%5D='.$arResult["ID"].'&set_filter=%CF%EE%E4%EE%E1%F0%E0%F2%FC&set_filter=Y&brand=1">'.$v["NAME"].'</a>';
				$cell++;
				if(strlen($v["NAME"]) > 18) $cell++;
			}
		}
	?>

		<?/*if($cell > 7) echo '<script language="javascript">$("#navigation_catalog").css("padding-bottom","10px"); $("#navigation_catalog").css("height","auto");</script>'*/?>
	</div>
</div>
	<?/*$APPLICATION->IncludeComponent(
	"bitrix:catalog.smart.filter", 
	"template1", 
	array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "3",
		"SECTION_ID" => "",
		"FILTER_NAME" => "arrFilter",
		"PRICE_CODE" => array(
		),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",
		"SAVE_IN_SESSION" => "N",
		"COMPONENT_TEMPLATE" => "template1",
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"HIDE_NOT_AVAILABLE" => "N",
		"TEMPLATE_THEME" => "",
		"FILTER_VIEW_MODE" => "horizontal",
		"DISPLAY_ELEMENT_COUNT" => "N",
		"SEF_MODE" => "N",
		"INSTANT_RELOAD" => "N",
		"PAGER_PARAMS_NAME" => "arrPager",
		"CONVERT_CURRENCY" => "N",
		"XML_EXPORT" => "N",
		"SECTION_TITLE" => "-",
		"SECTION_DESCRIPTION" => "-"
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);*/?>
<br/>
	<?if(count($arResult["LINKED_ELEMENTS"])>0):?>

				<?foreach($arResult["ITEMS_LINKED"] as $arElement):?>
			<div class="catalog-product-item row<? if(is_array($arElement["PROPERTIES"]["GROUPS"])) { echo ' groupped';}?>">
				<?if(!is_array($arElement["PROPERTIES"]["GROUPS"])): // ���� �� ������?>
					<?
					// ��������� ���� ����� ���������� ������ ~~~~~~~~~~~~
							$PriceCatalogProduct = CPrice::GetBasePrice($arElement["ID"]);
							$PriceProduct = $PriceDiscountProduct = $PriceCatalogProduct["PRICE"]; // ������� ���� ������
							
							$dbProductDiscounts = CCatalogDiscount::GetList(
										array("SORT" => "DESC"), 
										array("+PRODUCT_ID" => $arElement["ID"], "ACTIVE" => "Y", "!>ACTIVE_FROM" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")), "!<ACTIVE_TO" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")), "COUPON" => ""), false, false, 
										array("ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO", "RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE", "VALUE", "CURRENCY", "PRODUCT_ID")
								);
							while ($arProductDiscounts = $dbProductDiscounts->Fetch())
							{
								// ���� �� �������
								if($arProductDiscounts["VALUE_TYPE"] == "P")
									$PriceDiscountProduct = ($PriceProduct - ($PriceProduct * (($arProductDiscounts["VALUE"])/100))); 
								else
									$PriceDiscountProduct = $PriceProduct - $arProductDiscounts["VALUE"]; 
							}
					//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					if(substr_count($arElement["DETAIL_PAGE_URL"],"SITE_DIR") > 0) $arElement["DETAIL_PAGE_URL"] = $arElement["~DETAIL_PAGE_URL"];
					?>
					<?if($arElement['PREVIEW_IMG']['SRC']!=''):?>
						<div class="catalog-product-item__img medium-2">
							<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>" width="<?=$arElement["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_IMG"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" /></a>
						</div>
					<?else:?>
						<div class="catalog-product-item__img medium-2">
							<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img  src="<?=SITE_TEMPLATE_PATH?>/images/no_images.png" width="144" height="121" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" /></a>
						</div>
					<?endif;?>
						<div class="sizes-product medium-10 row">
              <div class="catalog-product-item__text column small-12">
    						<div class="title"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></div>
    						<div class="text"><?=$arElement['PREVIEW_TEXT']?></div>
    					</div>
							<div class="catalog-product-item__size clearfix">
                <div class="size-check">
									<span class="check-order <?=$arElement["PROPERTIES"]["PRESENSE"]["VALUE_XML_ID"]?>">
									<?if(strlen($arElement["PROPERTIES"]["PRESENSE"]["VALUE"])>0):?>
										<?=$arElement["PROPERTIES"]["PRESENSE"]["VALUE_ENUM"]; ?>
									<?endif;?>
									</span>
								</div>
								<div class="prices">
									<?
									$type_for_price = 180;
									if($PriceProduct != $PriceDiscountProduct):
										if(IntVal($PriceProduct) > 10000) $type_for_price = 210;
									endif;
									?>

									<?if($PriceProduct != $PriceDiscountProduct):?>
									  <div class="sale"><?=number_format($PriceProduct, 0,'',' ')?>&nbsp;���.</div>
										<div class="price"><?=number_format($PriceDiscountProduct, 0,'',' ')?>&nbsp;���.</div>
										
									<?else:?>
										<div class="price"><?=number_format($PriceProduct,0,'',' ')?>&nbsp;���.</div>
									<?endif;?>
								</div>

								<?if(/*$arElement["CAN_BUY"] && */($arElement["PROPERTIES"]["PRESENSE"]["VALUE_XML_ID"] != "outofstock")):?>
									<div class="extra cart-check">
										<?$PriceCatalogProductInList = CPrice::GetBasePrice($arElement["ID"]);?>
										<a href="/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=<?=$PriceCatalogProductInList["ID"]?>&PRODUCT_ID=<?=$arElement["ID"]?>" class="t_buy" style="float: none; margin-top:0px;" rel="nofollow"  onclick="return addToCart(this, 'catalog_list_image_<?=$arElement['ID']?>', 'list', '<?=GetMessage("CATALOG_IN_CART")?>');" id="catalog_add2cart_link_<?=$arElement['ID']?>">������</a>
									</div>
								<?else:?>
									<div class="extra">
										<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=GetMessage('CATALOG_MORE')?></a>
									</div>
								<?endif;?>

									<?/*td>
											<?$PriceCatalogProductInList = CPrice::GetBasePrice($arElement["ID"]);?>
											<a href="/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=<?=$PriceCatalogProduct["ID"]?>&PRODUCT_ID=<?=$arElement["ID"]?>" class="t_buy" style="float: none; margin-top:0px;" onclick="return addToCart(this, 'catalog_recomend_list_<?=$arElement['ID']?>', 'list', '������');">������</a>
									</td*/?>
							</div>
							
						</div>

					<?else: // ������?>
					
						<?if(count($arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"]) == 0){?>
							<?if($arElement['PREVIEW_IMG']['SRC']!=''):?>
								<div class="catalog-product-item__img medium-2">
									<img src="<?=$arElement["PREVIEW_IMG"]["SRC"]?>" width="<?=$arElement["PREVIEW_IMG"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_IMG"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" />
								</div>
							<?else:?>
								<div class="catalog-product-item__img medium-2">
									<img src="<?=SITE_TEMPLATE_PATH?>/images/no_images.png" width="136" height="121" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" id="catalog_list_image_<?=$arElement['ID']?>" />
								</div>
							<?endif;?>
						<?}else{?>
							<div class="catalog-product-item__img medium-2">
								<a href="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['src']?>" id="largeLinks_<?=$arElement["PROPERTIES"]["GROUPS"]["ID"][0]?>" alt="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>" title="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>" ><img id="largeImage_<?=$arElement["PROPERTIES"]["GROUPS"]["ID"][0]?>" src="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['src']?>" width="136" alt="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>" title="<?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"][0]['description']?>" /></a>
								<?
								if(count($arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"]) > 1){
									foreach($arElement["PROPERTIES"]["GROUPS"]["GROUPS_PHOTO"] as $key_photo=>$val_photo){
										if($key_photo == 0) continue;
									?>
										<img src="<?=$val_photo['src']?>" class="small" alt="<?=$val_photo['description']?>" title="<?=$val_photo['description']?>" rel="<?=$arElement["PROPERTIES"]["GROUPS"]["ID"][0]?>" />
								<?
									}
								}
								?>
							</div>
						<?}?>
					
					<?endif;?>
					
					
					
					
					<?if(is_array($arElement["PROPERTIES"]["GROUPS"])):?>
						<div class="sizes-product medium-10 row">
							<div class="catalog-product-item__text column small-12">
								<div class="title"><?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_NAME"]?></div>
								<div class="text"><?=$arElement["PROPERTIES"]["GROUPS"]["GROUPS_DESC"];?></div>
							</div>
						<?
						foreach($arElement["PROPERTIES"]["GROUPS"]["ID"] as $kRec=>$valRec):
							$resRecommend = CIBlockElement::GetByID($valRec)->GetNext();
							
							// ��������� ���� ����� ���������� ������ ~~~~~~~~~~~~
								$PriceCatalogProduct = CPrice::GetBasePrice($resRecommend["ID"]);
								$PriceProduct = $PriceDiscountProduct = $PriceCatalogProduct["PRICE"]; // ������� ���� ������
								
								$dbProductDiscounts = CCatalogDiscount::GetList(
											array("SORT" => "DESC"), 
											array("+PRODUCT_ID" => $resRecommend["ID"], "ACTIVE" => "Y", "!>ACTIVE_FROM" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")), "!<ACTIVE_TO" => $DB->FormatDate(date("Y-m-d H:i:s"), "YYYY-MM-DD HH:MI:SS", CSite::GetDateFormat("FULL")), "COUPON" => ""), false, false, 
											array("ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO", "RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE", "VALUE", "CURRENCY", "PRODUCT_ID")
									);
								while ($arProductDiscounts = $dbProductDiscounts->Fetch())
								{								
									// ���� �� �������
									if($arProductDiscounts["VALUE_TYPE"] == "P")
										$PriceDiscountProduct = ($PriceProduct - ($PriceProduct * ($arProductDiscounts["VALUE"]/100))); 
									else
										$PriceDiscountProduct = $PriceProduct - $arProductDiscounts["VALUE"]; 
								}
								
							//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							if(strlen($arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec]) > 22) $arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec] = substr($arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec],0,22).'...';
						?>
							<div class="catalog-product-item__size clearfix">
								<div class="size-name" id="catalog_recomend_list_<?=$resRecommend['ID']?>">
									<a href="<?=$arElement["PROPERTIES"]["GROUPS"]["DETAIL_PAGE_URL"][$kRec]?>" class="size-name_link"><?=$arElement["PROPERTIES"]["GROUPS"]["PRENAME"][$kRec];?></a>
								</div>
								<div class="size-check">
									<span class="check-order <?=$arElement["PROPERTIES"]["PRESENSE"]["VALUE_XML_ID"]?>">
									<?if(strlen($arElement["PROPERTIES"]["PRESENSE"]["VALUE"])>0):?>
										<?=$arElement["PROPERTIES"]["PRESENSE"]["VALUE_ENUM"]; ?>
									<?endif;?>
									</span>
								</div>
								<div class="prices">
									<?
									if($PriceProduct != $PriceDiscountProduct){// �������� ���� ������ � ������ ������
									?>
									<div class="sale"><?=number_format($PriceProduct, 0, '', ' ');?>&nbsp;���.</div>
									<div class="price"><?=number_format($PriceDiscountProduct, 0, '', ' ');?>&nbsp;���.</div>
									<?}else{?>
										<div class="price"><?=number_format($PriceProduct, 0, '', ' ');?>&nbsp;���.</div>
									<?}?>
								</div>
								<?if($arElement["PROPERTIES"]["GROUPS"]["PRESENSE"][$kRec]["VALUE_XML_ID"] != "outofstock"):?>
									<div class="extra cart-check">
										<a href="/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=<?=$PriceCatalogProduct["ID"]?>&PRODUCT_ID=<?=$resRecommend["ID"]?>" class="t_buy" onclick="return addToCart(this, 'catalog_recomend_list_<?=$resRecommend['ID']?>', 'list', '������');">������</a>
									</div>
								<?else:?>

									<div class="extra">
										<a href="<?=$arElement["PROPERTIES"]["GROUPS"]["DETAIL_PAGE_URL"][$kRec]?>"><?=GetMessage('CATALOG_MORE')?></a>
									</div>
								<?endif;?>
								<?/*td><a href="/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=<?=$PriceCatalogProduct["ID"]?>&PRODUCT_ID=<?=$resRecommend["ID"]?>" class="t_buy" onclick="return addToCart(this, 'catalog_recomend_list_<?=$resRecommend['ID']?>', 'list', '������');">������</a></td*/?>
							</div>
						<?endforeach;?>
						</div>
					<?endif;?>
					
		
			</div>

<?endforeach;?>

<?endif?>

