<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="news_page">
<h2>������</h2>
		<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		?>

		<div class="new_item" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
			<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
				<img src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" width="105" />
			<?elseif(is_array($arElement["DETAIL_PICTURE"])):?>
				<img src="<?=$arElement["DETAIL_PICTURE"]["SRC"]?>" width="105" />
			<?endif?>
			<div class="ni_text">
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?echo $arElement["NAME"]?></a>
				<?if($arElement["PREVIEW_TEXT"]):?>
					<p><?echo $arElement["PREVIEW_TEXT"];?></p>
				<?endif;?>
			</div>
		</div>

	<?endforeach; // foreach($arResult["ITEMS"] as $arElement):?>

</div>
