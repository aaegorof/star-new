<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(count($arResult["ITEMS"]) == 0){
	$APPLICATION->IncludeComponent(
	"bitrix:store.catalog.random",
	"special",
	Array(
		"DISPLAY_IMG_WIDTH" => "105",
		"DISPLAY_IMG_HEIGHT" => "70",
		"SHARPEN" => "50",
		"IBLOCK_TYPE_ID" => "catalog",
		"IBLOCK_ID" => array("3"),
		"PARENT_SECTION" => "",
		"RAND_COUNT" => "3",
		"DETAIL_URL" => "",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "180",
		"CACHE_GROUPS" => "Y",
	),
	false
);
	return "&nbsp;";
}
?>
<h2 style="font-size: 22px;">���������������</h2>
<div class="products-list">

<?foreach($arResult["ITEMS"] as $arElement):?>
	
	<div class="product-item">
		<div class="product-item__title"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement['NAME']?></a></div>
		<div class="product-item__img">
			<?if(is_array($arElement["PICTURE"])):?>
				
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img border="0" id="catalog_list_image_<?=$arElement['ID']?>" src="<?=$arElement["PICTURE"]["SRC"]?>" width="146" alt="<?=$arElement['NAME']?>" title="<?=$arElement['NAME']?>" /></a>
			<?else:?>
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img border="0" id="catalog_list_image_<?=$arElement['ID']?>" src="<?=SITE_TEMPLATE_PATH?>/images/no_images.png" width="146" height="116" alt="<?=$arElement['NAME']?>" title="<?=$arElement['NAME']?>" /></a>
			<?endif;?>
		</div>
		<div class="product-item__desc">
			<div class="prices">
				<?if ($arElement['bDiscount']):?>
					<div class="price price--sales"><?=round($arElement['PRICE']['PRICE']['PRICE'])?> ���.</div>	
				<?endif;?>
					<div class="price price--current"><?=round($arElement['PRICE']['DISCOUNT_PRICE'])?> ���.</div>
			</div>
			<div class="extra">
				<a href="<?echo "/include/add_to_basket.php?action=ADD2BASKET&quantity=1&price_id=".$arElement['PRICE']['PRICE']['ID']."&PRODUCT_ID=".$arElement['ID'];?>" rel="nofollow"  onclick="return addToCart(this, 'catalog_list_image_<?=$arElement['ID']?>', 'list', '�����');" id="catalog_add2cart_link_<?=$arElement['ID']?>">������</a>
			</div>
		</div>
<!--
			<a class="of_text" href="<?=$arElement["DETAIL_PAGE_URL"]?>">
				<? if (strlen($arElement["PREVIEW_TEXT"]) > 50):?>
					<?=substr($arElement["PREVIEW_TEXT"],0,50)?>[...]
				<?else:?>
					<?=$arElement["PREVIEW_TEXT"];?>
				<?endif?>
			</a>
-->
	</div>
<?endforeach;?>
</div>
