<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult["ITEMS"] as $key => $arElement){

	$arResult["ITEMS"][$key]['bDiscount'] = (is_array($arElement['PRICE']['DISCOUNT']) && count($arElement['PRICE']['DISCOUNT']) > 0);

	if ($arResult["ITEMS"][$key]['bDiscount'])
		$arResult["ITEMS"][$key]['PRICE']['DISCOUNT_PRICE_F'] = CurrencyFormat(
			$arElement['PRICE']['DISCOUNT_PRICE'], 
			$arElement['PRICE']['DISCOUNT']['CURRENCY']
		);

	if ($arElement['PRICE']['PRICE']['PRICE'])
		$arResult["ITEMS"][$key]['PRICE']['PRICE_F'] = CurrencyFormat(
			$arElement['PRICE']['PRICE']['PRICE'], 
			$arElement['PRICE']['PRICE']['CURRENCY']
		);

	$arResult["ITEMS"][$key]['DESCRIPTION'] = '';

	if ($arElement['PREVIEW_TEXT'])
	{
		$arResult["ITEMS"][$key]['DESCRIPTION'] = 
			$arElement['PREVIEW_TEXT_TYPE'] == 'html' 
			? $arElement['PREVIEW_TEXT'] 
			: htmlspecialchars($arElement['PREVIEW_TEXT']);
	}
	elseif ($arElement['DETAIL_TEXT'])
	{
		$arResult["ITEMS"][$key]['DESCRIPTION'] = 
			$arElement['DETAIL_TEXT_TYPE'] == 'html' 
			? $arElement['DETAIL_TEXT'] 
			: htmlspecialchars($arElement['DETAIL_TEXT']);
	}

	if(is_array($arElement["PICTURE"]))
	{
		$arFilter = '';
		if($arParams["SHARPEN"] != 0)
		{
			$arFilter = array(array("name" => "sharpen", "precision" => $arParams["SHARPEN"]));
		}
		$arFileTmp = CFile::ResizeImageGet(
			$arElement['PICTURE'],
			array("width" => $arParams["DISPLAY_IMG_WIDTH"], "height" => $arParams["DISPLAY_IMG_HEIGHT"]),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true, $arFilter
		);
		$arResult["ITEMS"][$key]['PICTURE_PREVIEW'] = array(
			'SRC' => $arFileTmp["src"],
			'WIDTH' => $arFileTmp["width"],
			'HEIGHT' => $arFileTmp["height"],
		);
	}
}
?>