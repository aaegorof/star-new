var gulp = require('gulp'),
    sassSCSS = require('gulp-sass'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer');
    notify = require("gulp-notify");
/*
    tinypng = require('gulp-tinypng-compress'),
    connect = require('gulp-connect'),
    babel = require('gulp-babel'),
    notify = require("gulp-notify");
*/



//минификация изображений
gulp.task('tinypng', function () {
	gulp.src('images/src/**/*.{png,jpg,jpeg}')
		.pipe(tinypng({
			key: 'wzh-97aFdlvuXooHhHd1YT0F_G-fsGFI',
			sigFile: 'images/.tinypng-sigs',
			log: true
		}))
		.pipe(gulp.dest('images'));
});



//sass
gulp.task('sass', function(){
	 gulp.src('sass/*.scss')
	.pipe(sassSCSS({errLogToConsole: false, sourceComments: 'map'}))
  .on('error', function(err) {
            notify().write(err);
            this.emit('end');
        })
  .pipe(autoprefixer())
	.pipe(gulp.dest('css'));
// 	.pipe(connect.reload());


});

gulp.task('babel', function () {
  gulp.src('js/test.js')
    .pipe(babel({
            presets: ['es2015']
        }))
    .pipe(gulp.dest('dist'))
});

gulp.task('html', function () {
  gulp.src('*.html')
    .pipe(connect.reload());
});

gulp.task('watch', function(){
  	gulp.watch('*.html', ['html'])
 	gulp.watch('sass/*.scss', ['sass'])
});


gulp.task('connect', function() {
  connect.server({
    //root: 'app',
    livereload: true
  });
});



gulp.task('default', [ 'sass', 'watch']);
