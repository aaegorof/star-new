$(document).foundation();

$('.banners').slick({
    dots: true,
    arrows: true,
    autoplay: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipeToSlide: true,
    autoplaySpeed: 5000, // Меняет скорость слайдов на главной страницы в милесекундах
    responsive: [{
        breakpoint: 1024,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
        }
    }, {
        breakpoint: 600,
        settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
        }
    }, {
        breakpoint: 480,
        settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
        }
    }]
});

// Этот паттерн создает фон для главного меню (сверху)
var bluePattern = Trianglify({
        width: $('.main-menu').width(),
        height: 200,
        cell_size: 50,
        variance: 0.3,
        color_space: 'hsl',
        x_colors: ['#0a5e9b','#31a9ef','#00b2d0','#0073b6'],  // Цвета по горизонтали перечислять через запятую в ковычках
        y_colors: ['#0a5e9b','#31a9ef','#00b2d0','#2071bd']  // Цвета по вертикали перечислять через запятую в ковычках
    });

// Сейчас этот паттерн не используется, он был в левом меню раньше, потом заменили картинкой
var darkPattern = Trianglify({
        width: 400,
        height: 1600,
        cell_size: 120,
        variance: 0.75,
        color_space: 'lab',
        x_colors: ['#14272e','#26414b','#0d6383','#406e7e','#14272e'],
        y_colors: ['#0d6383','#26414b','#14272e']
    });

/* $('.side-menu').css('background-image','url('+darkPattern.png()+')' ); */
$('.main-menu>ul').css('background-image', 'url('+bluePattern.png()+')');

$('#responsive-menu-button').sidr({
    name: 'sidr-main',
    source: '#navigation'
});


$('.section-description').each(function(){
	var that = $(this);
	console.log(that.height());
	if(that.outerHeight()>110){
		that.addClass('fadeover');
		that.append('<div class="over">Подробнее</div>');
	}
});

$('.over').click(function(){
	$(this).parent().removeClass('fadeover');
});

$('.news-items').slick({
    dots: false,
    arrows: false,
    autoplay: true,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    swipeToSlide: true,
    responsive: [{
        breakpoint: 800,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
        }
    }, {
        breakpoint: 600,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
        }
    }, {
        breakpoint: 480,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
        }
    }]
});

$('.about-us__companies').slick({
    dots: false,
    autoplay: true,
    infinite: true,
    speed: 300,
    slidesToShow: 8,
    slidesToScroll: 4,
    responsive: [{
        breakpoint: 800,
        settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            infinite: true,
        }
    }, {
        breakpoint: 600,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
        }
    }, {
        breakpoint: 480,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            centerMode: false,
        }
    }]
});

$('.switch-menu__item').click(function() {
    if (!($(this).hasClass('active'))) {
        $(".switch-menu__item").each(function(index) {
            $(this).removeClass("active");
        });
        numberMenu = ($(this).data("menu"));

        $(this).addClass("active");
        $(".side-menu-items ul").each(function(index) {
            $(this).removeClass("active");
            if ($(this).data("menu") == numberMenu) {
                $(this).addClass("active");
            }
        });
    }
});


$('.submenu_catalog a+ul').parent('li').addClass('sub-opener');

$('.li-expand').click(function(){
	$(this).parent('li').toggleClass('active');
});

/*
$(".side-menu .side-menu-items .opener").parent().addClass('opener').click(function() {
    $(this).toggleClass("active");
})
*/

