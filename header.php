<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// CJSCore::Init(array('jquery2'));
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?$APPLICATION->ShowTitle()?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/main.css">
	<link href="<?=SITE_TEMPLATE_PATH?>/fancybox/jquery.fancybox.css" type="text/css" rel="stylesheet">
	
<?/*if ($USER->GetID() != "15"): ?>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery.js"></script>
<?$APPLICATION->ShowHead();?>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/source.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
<link href="<?=SITE_TEMPLATE_PATH?>/jquery/fancybox/jquery.fancybox-1.3.1.css" type="text/css" rel="stylesheet">
	<?else:?>
<?endif;*/?>

<?$APPLICATION->ShowHead();?>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js!/source.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js!/cusel.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js!/select.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js!/easySlider.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/trianglify/0.4.0/trianglify.min.js"></script>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js!/script.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js!/jquery.validate.min.js"></script>
<script type="text/javascript">if (document.documentElement) { document.documentElement.id = "js" }</script>
</head>
<body>
<?$APPLICATION->ShowPanel();?>
	<header class="main-header">
		<div class="wrapper row align-middle">
			<div class="logo columns small-12 medium-3 large-2">
				<div class="logo__img">
					<a href="/"><img src="/images/logo-starbike.svg" width="100%" alt="������� � �����������"></a>
				</div>
				<div class="logo__desc">
					<a href="/">���������� �������� ����������</a>
				</div>
			</div>
			<div class="search-block columns small-12 medium-3 large-3 medium-order-6 small-order-6">
				<?$APPLICATION->IncludeComponent(
					"bitrix:search.title", 
					"store", 
					array(
						"NUM_CATEGORIES" => "1",
						"TOP_COUNT" => "5",
						"CHECK_DATES" => "N",
						"SHOW_OTHERS" => "Y",
						"PAGE" => "#SITE_DIR#search/",
						"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
						"CATEGORY_0" => array(
							0 => "iblock_catalog",
						),
						"CATEGORY_0_iblock_catalog" => array(
							0 => "3",
							1 => "4",
						),
						"CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
						"SHOW_INPUT" => "Y",
						"INPUT_ID" => "title-search-input",
						"CONTAINER_ID" => "search",
						"COMPONENT_TEMPLATE" => "store",
						"ORDER" => "date",
						"USE_LANGUAGE_GUESS" => "N"
					),
					false,
					array(
						"ACTIVE_COMPONENT" => "Y"
					)
				);?>
			</div>
			<div class="cart-block columns small-12 medium-3 large-3">
				<?
					$APPLICATION->IncludeComponent(
						"bitrix:sale.basket.basket.line", 
						".default", 
						array(
							"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
							"PATH_TO_PERSONAL" => SITE_DIR."personal/",
							"SHOW_PERSONAL_LINK" => "N",
							"SHOW_NUM_PRODUCTS" => "Y",
							"SHOW_TOTAL_PRICE" => "Y",
							"SHOW_EMPTY_VALUES" => "Y",
							"SHOW_AUTHOR" => "Y",
							"PATH_TO_REGISTER" => SITE_DIR."login/",
							"PATH_TO_PROFILE" => SITE_DIR."personal/",
							"SHOW_PRODUCTS" => "N",
							"POSITION_FIXED" => "Y",
							"POSITION_HORIZONTAL" => "right",
							"POSITION_VERTICAL" => "top"
						),
						false
					);
				?>
			</div>
      
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR."include/contacts.php"
					),
					false,
					array(
					"ACTIVE_COMPONENT" => "Y"
					)
				);?>
			
        </div>
		<nav class="main-menu" id="navigation">
<!--   		<div class="mate"><div class="white-bg"></div></div> -->
				<?
					$APPLICATION->IncludeComponent("bitrix:menu", "horizontal", array(
						"ROOT_MENU_TYPE" => "top",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => "",
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
						),
						false,
						array(
						"ACTIVE_COMPONENT" => "Y"
						)
					);
				?>
<!-- 				<div class="mate reverse"></div> -->  
				<a id="responsive-menu-button" href="#sidr-main">����</a>
			</nav>
	</header>
	<?if($_SERVER["PHP_SELF"] == "/index.php")://?>
	<?$APPLICATION->IncludeComponent("bitrix:news.list", "slider_page", Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	// ������ ������ ����
			"ADD_SECTIONS_CHAIN" => "N",	// �������� ������ � ������� ���������
			"AJAX_MODE" => "N",	// �������� ����� AJAX
			"AJAX_OPTION_ADDITIONAL" => "",	// �������������� �������������
			"AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
			"AJAX_OPTION_JUMP" => "Y",	// �������� ��������� � ������ ����������
			"AJAX_OPTION_STYLE" => "Y",	// �������� ��������� ������
			"CACHE_FILTER" => "N",	// ���������� ��� ������������� �������
			"CACHE_GROUPS" => "Y",	// ��������� ����� �������
			"CACHE_TIME" => "36000000",	// ����� ����������� (���.)
			"CACHE_TYPE" => "A",	// ��� �����������
			"CHECK_DATES" => "Y",	// ���������� ������ �������� �� ������ ������ ��������
			"COMPONENT_TEMPLATE" => "slider",
			"DETAIL_URL" => "",	// URL �������� ���������� ��������� (�� ��������� - �� �������� ���������)
			"DISPLAY_BOTTOM_PAGER" => "N",	// �������� ��� �������
			"DISPLAY_DATE" => "N",	// �������� ���� ��������
			"DISPLAY_NAME" => "N",	// �������� �������� ��������
			"DISPLAY_PICTURE" => "Y",	// �������� ����������� ��� ������
			"DISPLAY_PREVIEW_TEXT" => "N",	// �������� ����� ������
			"DISPLAY_TOP_PAGER" => "N",	// �������� ��� �������
			"FIELD_CODE" => array(	// ����
				0 => "PREVIEW_PICTURE",
				1 => "",
			),
			"FILTER_NAME" => "",	// ������
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// �������� ������, ���� ��� ���������� ��������
			"IBLOCK_ID" => "6",	// ��� ��������������� �����
			"IBLOCK_TYPE" => "slider",	// ��� ��������������� ����� (������������ ������ ��� ��������)
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// �������� �������� � ������� ���������
			"INCLUDE_SUBSECTIONS" => "N",	// ���������� �������� ����������� �������
			"MEDIA_PROPERTY" => "",	// �������� ��� ����������� �����
			"MESSAGE_404" => "",	// ��������� ��� ������ (�� ��������� �� ����������)
			"NEWS_COUNT" => "5",	// ���������� �������� �� ��������
			"PAGER_BASE_LINK_ENABLE" => "N",	// �������� ��������� ������
			"PAGER_DESC_NUMBERING" => "N",	// ������������ �������� ���������
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// ����� ����������� ������� ��� �������� ���������
			"PAGER_SHOW_ALL" => "N",	// ���������� ������ "���"
			"PAGER_SHOW_ALWAYS" => "N",	// �������� ������
			"PAGER_TEMPLATE" => ".default",	// ������ ������������ ���������
			"PAGER_TITLE" => "�������",	// �������� ���������
			"PARENT_SECTION" => "",	// ID �������
			"PARENT_SECTION_CODE" => "",	// ��� �������
			"PREVIEW_TRUNCATE_LEN" => "",	// ������������ ����� ������ ��� ������ (������ ��� ���� �����)
			"PROPERTY_CODE" => array(	// ��������
				0 => "LINK",
				1 => "",
			),
			"SEARCH_PAGE" => "/search/",	// ���� � �������� ������
			"SET_BROWSER_TITLE" => "N",	// ������������� ��������� ���� ��������
			"SET_LAST_MODIFIED" => "N",	// ������������� � ���������� ������ ����� ����������� ��������
			"SET_META_DESCRIPTION" => "N",	// ������������� �������� ��������
			"SET_META_KEYWORDS" => "N",	// ������������� �������� ����� ��������
			"SET_STATUS_404" => "N",	// ������������� ������ 404
			"SET_TITLE" => "N",	// ������������� ��������� ��������
			"SHOW_404" => "N",	// ����� ����������� ��������
			"SLIDER_PROPERTY" => "",	// �������� � ������������� ��� ��������
			"SORT_BY1" => "SORT",	// ���� ��� ������ ���������� ��������
			"SORT_BY2" => "ACTIVE_FROM",	// ���� ��� ������ ���������� ��������
			"SORT_ORDER1" => "ASC",	// ����������� ��� ������ ���������� ��������
			"SORT_ORDER2" => "DESC",	// ����������� ��� ������ ���������� ��������
			"TEMPLATE_THEME" => "",	// �������� ����
			"USE_RATING" => "N",	// ��������� �����������
			"USE_SHARE" => "N",	// ���������� ������ ���. ��������
		),
		false,
		array(
		"ACTIVE_COMPONENT" => "Y"
		)
	);?>

	<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
		"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_TEMPLATE_PATH."/include/menu-cat.php"
		),
		false,
		array(
		"ACTIVE_COMPONENT" => "Y"
		)
	);?>
	<?endif;?>
	<div class="page-content wrapper clearfix">
        <div class="left_column">
            <div class="side-menu">
                <div class="switch-menu">
                    <div data-menu="1" class="switch-menu__item skew-right active"><div>�������</div></div>
                    <div data-menu="2" class="switch-menu__item skew-left"><div>������</div></div>
                </div>
                <div class="side-menu-items">
					<?$APPLICATION->IncludeComponent(
						"bitrix:catalog.section.list", 
						"tree",
						array(
							"IBLOCK_TYPE" => "left_menu_catalog",
							"IBLOCK_ID" => "8",
							"SECTION_ID" => "",
							"SECTION_CODE" => "",
							"SECTION_URL" => "",
							"COUNT_ELEMENTS" => "Y",
							"TOP_DEPTH" => "6",
							"SECTION_FIELDS" => array(
								0 => "",
								1 => "",
							),
							"SECTION_USER_FIELDS" => array(
								0 => "",
								1 => "",
							),
							"ADD_SECTIONS_CHAIN" => "N",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "3600000",
							"CACHE_GROUPS" => "N",
							"COMPONENT_TEMPLATE" => "tree"
						),
						false
					);?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:news.list", 
							"left_menu_brand", 
							array(
								"IBLOCK_TYPE" => "catalog",
								"IBLOCK_ID" => "4",
								"NEWS_COUNT" => "400",
								"SORT_BY1" => "NAME",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "",
								"SORT_ORDER2" => "",
								"FILTER_NAME" => "",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
									0 => "",
									1 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "Y",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "3600000",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_STATUS_404" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "Y",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "ÃÃ®Ã¢Ã®Ã±Ã²Ã¨",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_TEMPLATE" => "",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"DISPLAY_DATE" => "Y",
								"DISPLAY_NAME" => "Y",
								"DISPLAY_PICTURE" => "Y",
								"DISPLAY_PREVIEW_TEXT" => "Y",
								"AJAX_OPTION_ADDITIONAL" => "",
								"SET_BROWSER_TITLE" => "Y",
								"SET_META_KEYWORDS" => "Y",
								"SET_META_DESCRIPTION" => "Y"
							),
							false,
							array(
								"ACTIVE_COMPONENT" => "Y"
							)
						);?>
                </div>
            </div>
            <div class="left_column__block left_column__block--contacts">
				<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_DIR."include/contacts-left-column-new.php",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?>                
            </div>
            <div class="left_column__block">
                <?$APPLICATION->IncludeComponent(
	"primepix:vkontakte.group", 
	".default", 
	array(
		"ID_GROUP" => "31038751",
		"TYPE_FORM" => "0",
		"WIDTH_FORM" => "288",
		"HEIGHT_FORM" => "300",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
            </div>
        </div>
        <div class="center_column"<?if(substr_count($_SERVER["REQUEST_URI"], "/brand/")>0):?> style="margin-top: -35px;"<?endif;?>>
				
					
<!-- 					<?
					$STATUS_HIDE = 1;
					$arr_link_substr = array('brand','news','catalog','contacts','personal','special','search');
					foreach($arr_link_substr as $v_sub) if(substr_count($_SERVER["REQUEST_URI"],$v_sub)!=0) $STATUS_HIDE = 0;
					if($_SERVER["PHP_SELF"] == '/index.php') $STATUS_HIDE = 0;
					if($STATUS_HIDE){
						$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", array(
							"START_FROM" => "1",
							"PATH" => "",
							"SITE_ID" => "-"
							),
							false,
							Array('HIDE_ICONS' => 'Y')
						);
						echo '<div class="b-content">';
					}
					?> -->