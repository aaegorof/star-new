$('.banners').slick({
    dots: true,
    arrows: true,
    autoplay: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipeToSlide: true,
    responsive: [{
        breakpoint: 1024,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
        }
    }, {
        breakpoint: 600,
        settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
        }
    }, {
        breakpoint: 480,
        settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
        }
    }]
});

$('#responsive-menu-button').sidr({
    name: 'sidr-main',
    source: '#navigation'
});

$('.news-items').slick({
    dots: false,
    arrows: false,
    autoplay: true,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    swipeToSlide: true,
    responsive: [{
        breakpoint: 800,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
        }
    }, {
        breakpoint: 600,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
        }
    }, {
        breakpoint: 480,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
        }
    }]
});

$('.about-us__companies').slick({
    dots: false,
    arrows: false,
    autoplay: true,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 6,
    swipeToSlide: true,
    responsive: [{
        breakpoint: 800,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
        }
    }, {
        breakpoint: 600,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
        }
    }, {
        breakpoint: 480,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
        }
    }]
});

$('.switch-menu__item').click(function() {
    if (!($(this).hasClass('active'))) {
        $(".switch-menu__item").each(function(index) {
            $(this).removeClass("active");
        });
        numberMenu = ($(this).data("menu"));

        $(this).addClass("active");
        $(".side-menu-items ul").each(function(index) {
            $(this).removeClass("active");
            if ($(this).data("menu") == numberMenu) {
                $(this).addClass("active");
            }
        });
    }
})



$(".side-menu .side-menu-items ul li").click(function() {
    $(this).toggleClass("active");
})

